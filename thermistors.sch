EESchema Schematic File Version 4
LIBS:RAMPS_1-41-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 5 6
Title "Reprap Arduino Mega Pololu Shield"
Date "2018-08-21"
Rev "1.4"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5150 4200 4950 4200
Wire Wire Line
	4950 4200 4950 4400
Wire Wire Line
	4950 3000 4950 4200
Wire Wire Line
	5950 3000 4950 3000
Connection ~ 4950 4200
Text Label 5150 4200 0    10   ~ 0
GND
Wire Wire Line
	6050 4300 5750 4300
Wire Wire Line
	5750 4300 5750 3200
Wire Wire Line
	5750 3200 6150 3200
Wire Wire Line
	5750 4400 5750 4300
Wire Wire Line
	6150 3200 6150 3000
Connection ~ 5750 4300
Text Label 6050 4300 0    10   ~ 0
GND
Wire Wire Line
	6650 4400 6650 4300
Wire Wire Line
	6650 4300 6650 3100
Wire Wire Line
	6650 3100 6350 3100
Wire Wire Line
	6350 3100 6350 3000
Wire Wire Line
	6850 4300 6650 4300
Connection ~ 6650 4300
Text Label 6650 4400 0    10   ~ 0
GND
Wire Wire Line
	5150 3800 5150 3900
Wire Wire Line
	5450 3800 5150 3800
Wire Wire Line
	5450 3800 5450 4700
Wire Wire Line
	5450 3800 5450 3100
Wire Wire Line
	5450 3100 6050 3100
Wire Wire Line
	6050 3100 6050 3000
Connection ~ 5150 3800
Connection ~ 5450 3800
Text Label 5450 4700 1    70   ~ 0
THERM0
Wire Wire Line
	6050 3800 6050 4000
Wire Wire Line
	6050 3800 6350 3800
Wire Wire Line
	6350 3800 6350 3200
Wire Wire Line
	6350 3800 6350 4600
Wire Wire Line
	6350 3200 6250 3200
Wire Wire Line
	6250 3200 6250 3000
Connection ~ 6050 3800
Connection ~ 6350 3800
Text Label 6350 4600 1    70   ~ 0
THERM1
Wire Wire Line
	7350 4600 7350 3800
Wire Wire Line
	7350 3800 7350 3000
Wire Wire Line
	7350 3000 6450 3000
Wire Wire Line
	6850 4000 6850 3800
Wire Wire Line
	7350 3800 6850 3800
Connection ~ 6850 3800
Connection ~ 7350 3800
Text Label 7350 4600 1    70   ~ 0
THERM2
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R?
U 1 0 5BDBB9B4
P 6050 3600
AR Path="/5BDBB9B4" Ref="R?"  Part="1" 
AR Path="/5BD9CED8/5BDBB9B4" Ref="R1"  Part="1" 
F 0 "R1" H 5900 3659 59  0000 L BNN
F 1 "4.7K" H 5900 3470 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 6050 3600 50  0001 C CNN
F 3 "" H 6050 3600 50  0001 C CNN
	1    6050 3600
	0    -1   -1   0   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:CPOL-US153CLV-0405 C?
U 1 0 5BDBB9BB
P 6050 4100
AR Path="/5BDBB9BB" Ref="C?"  Part="1" 
AR Path="/5BD9CED8/5BDBB9BB" Ref="C8"  Part="1" 
F 0 "C8" H 6090 4125 59  0000 L BNN
F 1 "10uF" H 6090 3935 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:153CLV-0405" H 6050 4100 50  0001 C CNN
F 3 "" H 6050 4100 50  0001 C CNN
	1    6050 4100
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BDBB9C2
P 5750 4500
F 0 "#GND?" H 5750 4500 50  0001 C CNN
F 1 "GND" H 5650 4400 59  0000 L BNN
F 2 "" H 5750 4500 50  0001 C CNN
F 3 "" H 5750 4500 50  0001 C CNN
	1    5750 4500
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BDBB9C8
P 6050 3300
F 0 "#P+?" H 6050 3300 50  0001 C CNN
F 1 "VCC" V 5950 3200 59  0000 L BNN
F 2 "" H 6050 3300 50  0001 C CNN
F 3 "" H 6050 3300 50  0001 C CNN
	1    6050 3300
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BDBB9CE
P 5150 3300
F 0 "#P+?" H 5150 3300 50  0001 C CNN
F 1 "VCC" V 5050 3200 59  0000 L BNN
F 2 "" H 5150 3300 50  0001 C CNN
F 3 "" H 5150 3300 50  0001 C CNN
	1    5150 3300
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R?
U 1 0 5BDBB9D4
P 5150 3600
AR Path="/5BDBB9D4" Ref="R?"  Part="1" 
AR Path="/5BD9CED8/5BDBB9D4" Ref="R7"  Part="1" 
F 0 "R7" H 5000 3659 59  0000 L BNN
F 1 "4.7K" H 5000 3470 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 5150 3600 50  0001 C CNN
F 3 "" H 5150 3600 50  0001 C CNN
	1    5150 3600
	0    -1   -1   0   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:CPOL-US153CLV-0405 C?
U 1 0 5BDBB9DB
P 5150 4000
AR Path="/5BDBB9DB" Ref="C?"  Part="1" 
AR Path="/5BD9CED8/5BDBB9DB" Ref="C5"  Part="1" 
F 0 "C5" H 5190 4025 59  0000 L BNN
F 1 "10uF" H 5190 3835 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:153CLV-0405" H 5150 4000 50  0001 C CNN
F 3 "" H 5150 4000 50  0001 C CNN
	1    5150 4000
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BDBB9E2
P 4950 4500
F 0 "#GND?" H 4950 4500 50  0001 C CNN
F 1 "GND" H 4850 4400 59  0000 L BNN
F 2 "" H 4950 4500 50  0001 C CNN
F 3 "" H 4950 4500 50  0001 C CNN
	1    4950 4500
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:M06SIP JP?
U 1 0 5BDBB9E8
P 6150 2800
AR Path="/5BDBB9E8" Ref="JP?"  Part="1" 
AR Path="/5BD9CED8/5BDBB9E8" Ref="JP7"  Part="1" 
F 0 "JP7" H 5950 3230 59  0000 L BNN
F 1 "M06SIP" H 5950 2400 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:1X06" H 6150 2800 50  0001 C CNN
F 3 "" H 6150 2800 50  0001 C CNN
	1    6150 2800
	0    1    1    0   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R?
U 1 0 5BDBB9EF
P 6850 3600
AR Path="/5BDBB9EF" Ref="R?"  Part="1" 
AR Path="/5BD9CED8/5BDBB9EF" Ref="R11"  Part="1" 
F 0 "R11" H 6700 3659 59  0000 L BNN
F 1 "4.7K" H 6700 3470 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 6850 3600 50  0001 C CNN
F 3 "" H 6850 3600 50  0001 C CNN
	1    6850 3600
	0    -1   -1   0   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:CPOL-US153CLV-0405 C?
U 1 0 5BDBB9F6
P 6850 4100
AR Path="/5BDBB9F6" Ref="C?"  Part="1" 
AR Path="/5BD9CED8/5BDBB9F6" Ref="C1"  Part="1" 
F 0 "C1" H 6890 4125 59  0000 L BNN
F 1 "10uF" H 6890 3935 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:153CLV-0405" H 6850 4100 50  0001 C CNN
F 3 "" H 6850 4100 50  0001 C CNN
	1    6850 4100
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BDBB9FD
P 6650 4500
F 0 "#GND?" H 6650 4500 50  0001 C CNN
F 1 "GND" H 6550 4400 59  0000 L BNN
F 2 "" H 6650 4500 50  0001 C CNN
F 3 "" H 6650 4500 50  0001 C CNN
	1    6650 4500
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BDBBA03
P 6850 3300
F 0 "#P+?" H 6850 3300 50  0001 C CNN
F 1 "VCC" V 6750 3200 59  0000 L BNN
F 2 "" H 6850 3300 50  0001 C CNN
F 3 "" H 6850 3300 50  0001 C CNN
	1    6850 3300
	1    0    0    -1  
$EndComp
Text Notes 4850 4200 0    85   ~ 0
Thermistor 0
Text Notes 5650 4200 0    85   ~ 0
Thermistor 1
Text Notes 6550 4200 0    85   ~ 0
Thermistor 2
$EndSCHEMATC
