EESchema Schematic File Version 4
LIBS:RAMPS_1-41-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 2 6
Title "Reprap Arduino Mega Pololu Shield"
Date "2018-08-21"
Rev "1.4"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	8750 4550 9250 4550
Wire Wire Line
	9250 3750 8850 3750
Wire Wire Line
	8850 3750 8850 3950
Wire Wire Line
	8850 3950 8750 3950
Wire Wire Line
	9250 4550 9250 3750
Wire Wire Line
	9250 4550 9250 4750
Connection ~ 9250 4550
Text Label 8750 4550 0    10   ~ 0
GND
Wire Wire Line
	6450 3450 6850 3450
Text Label 6450 3450 0    10   ~ 0
GND
Wire Wire Line
	7150 4150 6550 4150
Wire Wire Line
	7150 4350 7150 4250
Wire Wire Line
	9050 4750 9050 4850
Wire Wire Line
	8850 4850 8850 4450
Wire Wire Line
	8850 4450 8750 4450
Text Label 9050 4750 0    10   ~ 0
VCC
Wire Wire Line
	6250 4150 5950 4150
Wire Wire Line
	5950 4050 5950 4150
Wire Wire Line
	6250 4050 5950 4050
Wire Wire Line
	5950 3950 5950 4050
Wire Wire Line
	5950 3950 5950 3750
Wire Wire Line
	6250 3950 5950 3950
Connection ~ 5950 4050
Connection ~ 5950 3950
Text Label 6250 4150 0    10   ~ 0
VCC
Wire Wire Line
	8750 3550 8750 3850
Text Label 8750 3550 0    10   ~ 0
+12V
Wire Wire Line
	7150 4450 6950 4450
Text Label 6950 4450 0    70   ~ 0
E0-STEP
Wire Wire Line
	7150 3850 6950 3850
Wire Wire Line
	7150 3750 7150 3850
Connection ~ 7150 3850
Text Label 6950 3850 0    70   ~ 0
E0-EN
Wire Wire Line
	7150 4550 6950 4550
Text Label 6950 4550 0    70   ~ 0
E0-DIR
Wire Wire Line
	6550 3950 6850 3950
Wire Wire Line
	6850 3950 7150 3950
Wire Wire Line
	6850 3850 6850 3950
Connection ~ 6850 3950
Wire Wire Line
	7150 4050 6550 4050
Wire Wire Line
	9050 4050 8750 4050
Wire Wire Line
	9050 4150 8750 4150
Wire Wire Line
	9050 4250 8750 4250
Wire Wire Line
	9050 4350 8750 4350
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+0101
U 1 0 5BB19614
P 9050 4650
F 0 "#P+0101" H 9050 4650 50  0001 C CNN
F 1 "VCC" V 8950 4550 59  0000 L BNN
F 2 "" H 9050 4650 50  0001 C CNN
F 3 "" H 9050 4650 50  0001 C CNN
	1    9050 4650
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:+12V #P+0102
U 1 0 5BB1961A
P 8750 3450
F 0 "#P+0102" H 8750 3450 50  0001 C CNN
F 1 "+12V" V 8650 3250 59  0000 L BNN
F 2 "" H 8750 3450 50  0001 C CNN
F 3 "" H 8750 3450 50  0001 C CNN
	1    8750 3450
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND0101
U 1 0 5BB19620
P 9250 4850
F 0 "#GND0101" H 9250 4850 50  0001 C CNN
F 1 "GND" H 9150 4750 59  0000 L BNN
F 2 "" H 9250 4850 50  0001 C CNN
F 3 "" H 9250 4850 50  0001 C CNN
	1    9250 4850
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND0102
U 1 0 5BB19626
P 6450 3550
F 0 "#GND0102" H 6450 3550 50  0001 C CNN
F 1 "GND" H 6350 3450 59  0000 L BNN
F 2 "" H 6450 3550 50  0001 C CNN
F 3 "" H 6450 3550 50  0001 C CNN
	1    6450 3550
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R6
U 1 0 5BB1962C
P 6850 3650
F 0 "R6" H 6700 3709 59  0000 L BNN
F 1 "100k" H 6700 3520 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 6850 3650 50  0001 C CNN
F 3 "" H 6850 3650 50  0001 C CNN
	1    6850 3650
	0    1    1    0   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:PINHD-2X3 JP2
U 1 0 5BB19633
P 6450 4050
F 0 "JP2" H 6200 4275 59  0000 L BNN
F 1 "PINHD-2X3" H 6200 3750 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:2X03" H 6450 4050 50  0001 C CNN
F 3 "" H 6450 4050 50  0001 C CNN
	1    6450 4050
	-1   0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:PINHD-1X4 E0-MOT1
U 1 0 5BB1963A
P 9150 4150
F 0 "E0-MOT1" H 8900 4375 59  0000 L BNN
F 1 "PINHD-1X4" H 8900 3750 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:1X04" H 9150 4150 50  0001 C CNN
F 3 "" H 9150 4150 50  0001 C CNN
	1    9150 4150
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+0103
U 1 0 5BB19641
P 5950 3650
F 0 "#P+0103" H 5950 3650 50  0001 C CNN
F 1 "VCC" V 5850 3550 59  0000 L BNN
F 2 "" H 5950 3650 50  0001 C CNN
F 3 "" H 5950 3650 50  0001 C CNN
	1    5950 3650
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:A4988BREAKOUT U$?
U 1 0 5BB19647
P 8150 3950
AR Path="/5BB19647" Ref="U$?"  Part="1" 
AR Path="/5BA9E9B7/5BB19647" Ref="U$4"  Part="1" 
F 0 "U$4" H 8150 3950 50  0001 C CNN
F 1 "A4988BREAKOUT" H 8150 3950 50  0001 C CNN
F 2 "RAMPS_1-41_eagle9:A4988BREAKOUT" H 8150 3950 50  0001 C CNN
F 3 "" H 8150 3950 50  0001 C CNN
	1    8150 3950
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R16
U 1 0 5BB1964D
P 7150 3550
F 0 "R16" H 7000 3609 59  0000 L BNN
F 1 "10k" H 7000 3420 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 7150 3550 50  0001 C CNN
F 3 "" H 7150 3550 50  0001 C CNN
	1    7150 3550
	0    1    1    0   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+0104
U 1 0 5BB19654
P 7150 3250
F 0 "#P+0104" H 7150 3250 50  0001 C CNN
F 1 "VCC" V 7050 3150 59  0000 L BNN
F 2 "" H 7150 3250 50  0001 C CNN
F 3 "" H 7150 3250 50  0001 C CNN
	1    7150 3250
	1    0    0    -1  
$EndComp
Text Notes 7550 3450 0    85   ~ 0
E0
Wire Wire Line
	8850 4850 9050 4850
Wire Wire Line
	4500 5950 3900 5950
Wire Wire Line
	4500 6150 4500 5950
Wire Wire Line
	4500 5950 4500 5150
Wire Wire Line
	4500 5150 4000 5150
Wire Wire Line
	4000 5150 4000 5350
Wire Wire Line
	4000 5350 3900 5350
Connection ~ 4500 5950
Text Label 4500 5950 0    10   ~ 0
GND
Wire Wire Line
	1900 4850 1500 4850
Text Label 1900 4850 0    10   ~ 0
GND
Wire Wire Line
	3900 5850 4000 5850
Wire Wire Line
	4000 5850 4000 6250
Wire Wire Line
	4300 6250 4300 6150
Text Label 3900 5850 0    10   ~ 0
VCC
Wire Wire Line
	1100 5550 1400 5550
Wire Wire Line
	1100 5450 1400 5450
Wire Wire Line
	1100 5450 1100 5550
Wire Wire Line
	1100 5350 1400 5350
Wire Wire Line
	1100 5350 1100 5450
Wire Wire Line
	1100 5050 1100 5350
Connection ~ 1100 5450
Connection ~ 1100 5350
Text Label 1100 5550 0    10   ~ 0
VCC
Wire Wire Line
	3900 4950 3900 5250
Text Label 3900 4950 0    10   ~ 0
+12V
Wire Wire Line
	2300 5650 2300 5750
Wire Wire Line
	2300 5350 1900 5350
Wire Wire Line
	1900 5350 1700 5350
Wire Wire Line
	1900 5250 1900 5350
Connection ~ 1900 5350
Wire Wire Line
	1800 5850 2300 5850
Text Label 1800 5850 0    70   ~ 0
E1-STEP
Wire Wire Line
	2300 5250 2200 5250
Wire Wire Line
	2200 5250 2100 5250
Wire Wire Line
	2200 5150 2200 5250
Connection ~ 2200 5250
Text Label 2100 5250 0    70   ~ 0
E1-EN
Wire Wire Line
	2300 5550 1700 5550
Wire Wire Line
	2300 5450 1700 5450
Wire Wire Line
	4200 5450 3900 5450
Wire Wire Line
	4200 5550 3900 5550
Wire Wire Line
	4200 5650 3900 5650
Wire Wire Line
	4200 5750 3900 5750
Wire Wire Line
	2300 5950 1800 5950
Text Label 1800 5950 0    70   ~ 0
E1-DIR
$Comp
L RAMPS_1-41_eagle9-eagle-import:+12V #P+?
U 1 0 5BBC16D7
P 3900 4850
AR Path="/5BBC16D7" Ref="#P+?"  Part="1" 
AR Path="/5BA9E9B7/5BBC16D7" Ref="#P+0105"  Part="1" 
F 0 "#P+0105" H 3900 4850 50  0001 C CNN
F 1 "+12V" V 3800 4650 59  0000 L BNN
F 2 "" H 3900 4850 50  0001 C CNN
F 3 "" H 3900 4850 50  0001 C CNN
	1    3900 4850
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BBC16DD
P 4500 6250
AR Path="/5BBC16DD" Ref="#GND?"  Part="1" 
AR Path="/5BA9E9B7/5BBC16DD" Ref="#GND0103"  Part="1" 
F 0 "#GND0103" H 4500 6250 50  0001 C CNN
F 1 "GND" H 4400 6150 59  0000 L BNN
F 2 "" H 4500 6250 50  0001 C CNN
F 3 "" H 4500 6250 50  0001 C CNN
	1    4500 6250
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BBC16E3
P 4300 6050
AR Path="/5BBC16E3" Ref="#P+?"  Part="1" 
AR Path="/5BA9E9B7/5BBC16E3" Ref="#P+0106"  Part="1" 
F 0 "#P+0106" H 4300 6050 50  0001 C CNN
F 1 "VCC" V 4200 5950 59  0000 L BNN
F 2 "" H 4300 6050 50  0001 C CNN
F 3 "" H 4300 6050 50  0001 C CNN
	1    4300 6050
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BBC16E9
P 1500 4950
AR Path="/5BBC16E9" Ref="#GND?"  Part="1" 
AR Path="/5BA9E9B7/5BBC16E9" Ref="#GND0104"  Part="1" 
F 0 "#GND0104" H 1500 4950 50  0001 C CNN
F 1 "GND" H 1400 4850 59  0000 L BNN
F 2 "" H 1500 4950 50  0001 C CNN
F 3 "" H 1500 4950 50  0001 C CNN
	1    1500 4950
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:PINHD-2X3 JP?
U 1 0 5BBC16EF
P 1600 5450
AR Path="/5BBC16EF" Ref="JP?"  Part="1" 
AR Path="/5BA9E9B7/5BBC16EF" Ref="JP3"  Part="1" 
F 0 "JP3" H 1350 5675 59  0000 L BNN
F 1 "PINHD-2X3" H 1350 5150 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:2X03" H 1600 5450 50  0001 C CNN
F 3 "" H 1600 5450 50  0001 C CNN
	1    1600 5450
	-1   0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R?
U 1 0 5BBC16F6
P 1900 5050
AR Path="/5BBC16F6" Ref="R?"  Part="1" 
AR Path="/5BA9E9B7/5BBC16F6" Ref="R3"  Part="1" 
F 0 "R3" H 1750 5109 59  0000 L BNN
F 1 "100k" H 1750 4920 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 1900 5050 50  0001 C CNN
F 3 "" H 1900 5050 50  0001 C CNN
	1    1900 5050
	0    -1   -1   0   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:PINHD-1X4 E1-MOT?
U 1 0 5BBC16FD
P 4300 5550
AR Path="/5BBC16FD" Ref="E1-MOT?"  Part="1" 
AR Path="/5BA9E9B7/5BBC16FD" Ref="E1-MOT1"  Part="1" 
F 0 "E1-MOT1" H 4050 5775 59  0000 L BNN
F 1 "PINHD-1X4" H 4050 5150 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:1X04" H 4300 5550 50  0001 C CNN
F 3 "" H 4300 5550 50  0001 C CNN
	1    4300 5550
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BBC1704
P 1100 4950
AR Path="/5BBC1704" Ref="#P+?"  Part="1" 
AR Path="/5BA9E9B7/5BBC1704" Ref="#P+0107"  Part="1" 
F 0 "#P+0107" H 1100 4950 50  0001 C CNN
F 1 "VCC" V 1000 4850 59  0000 L BNN
F 2 "" H 1100 4950 50  0001 C CNN
F 3 "" H 1100 4950 50  0001 C CNN
	1    1100 4950
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:A4988BREAKOUT U$?
U 1 0 5BBC170A
P 3300 5350
AR Path="/5BBC170A" Ref="U$?"  Part="1" 
AR Path="/5BA9E9B7/5BBC170A" Ref="U$5"  Part="1" 
F 0 "U$5" H 3300 5350 50  0001 C CNN
F 1 "A4988BREAKOUT" H 3300 5350 50  0001 C CNN
F 2 "RAMPS_1-41_eagle9:A4988BREAKOUT" H 3300 5350 50  0001 C CNN
F 3 "" H 3300 5350 50  0001 C CNN
	1    3300 5350
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R?
U 1 0 5BBC1710
P 2200 4950
AR Path="/5BBC1710" Ref="R?"  Part="1" 
AR Path="/5BA9E9B7/5BBC1710" Ref="R17"  Part="1" 
F 0 "R17" H 2050 5009 59  0000 L BNN
F 1 "10k" H 2050 4820 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 2200 4950 50  0001 C CNN
F 3 "" H 2200 4950 50  0001 C CNN
	1    2200 4950
	0    1    1    0   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BBC1717
P 2200 4650
AR Path="/5BBC1717" Ref="#P+?"  Part="1" 
AR Path="/5BA9E9B7/5BBC1717" Ref="#P+0108"  Part="1" 
F 0 "#P+0108" H 2200 4650 50  0001 C CNN
F 1 "VCC" V 2100 4550 59  0000 L BNN
F 2 "" H 2200 4650 50  0001 C CNN
F 3 "" H 2200 4650 50  0001 C CNN
	1    2200 4650
	1    0    0    -1  
$EndComp
Text Notes 2700 4850 0    85   ~ 0
E1
Wire Wire Line
	4000 6250 4300 6250
Wire Wire Line
	4250 2250 3750 2250
Wire Wire Line
	4250 2350 4250 2250
Wire Wire Line
	4250 2250 4250 1450
Wire Wire Line
	4250 1450 3850 1450
Wire Wire Line
	3850 1450 3850 1650
Wire Wire Line
	3850 1650 3750 1650
Connection ~ 4250 2250
Text Label 4250 2250 0    10   ~ 0
GND
Wire Wire Line
	1550 1050 1850 1050
Text Label 1550 1050 0    10   ~ 0
GND
Wire Wire Line
	4050 2550 4050 2450
Wire Wire Line
	3850 2550 4050 2550
Wire Wire Line
	3750 2150 3850 2150
Wire Wire Line
	3850 2150 3850 2550
Text Label 4050 2550 0    10   ~ 0
VCC
Wire Wire Line
	1050 1650 1050 1450
Wire Wire Line
	1350 1850 1050 1850
Wire Wire Line
	1350 1750 1050 1750
Wire Wire Line
	1050 1750 1050 1850
Wire Wire Line
	1050 1750 1050 1650
Wire Wire Line
	1350 1650 1050 1650
Connection ~ 1050 1750
Connection ~ 1050 1650
Text Label 1050 1650 0    10   ~ 0
VCC
Wire Wire Line
	1650 1850 2150 1850
Wire Wire Line
	2150 1950 2150 2050
Wire Wire Line
	3750 1250 3750 1550
Text Label 3750 1250 0    10   ~ 0
+12V
Wire Wire Line
	2150 1550 1950 1550
Wire Wire Line
	2150 1450 2150 1550
Connection ~ 2150 1550
Text Label 1950 1550 0    70   ~ 0
X-EN
Wire Wire Line
	2150 2150 1950 2150
Text Label 1950 2150 0    70   ~ 0
X-STEP
Wire Wire Line
	2150 2250 1950 2250
Text Label 1950 2250 0    70   ~ 0
X-DIR
Wire Wire Line
	1650 1650 1850 1650
Wire Wire Line
	1850 1650 2150 1650
Wire Wire Line
	1850 1450 1850 1650
Connection ~ 1850 1650
Wire Wire Line
	2150 1750 1650 1750
Wire Wire Line
	4050 1750 3750 1750
Wire Wire Line
	4050 1850 3750 1850
Wire Wire Line
	4050 1950 3750 1950
Wire Wire Line
	4050 2050 3750 2050
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BC1F8BE
P 4250 2450
AR Path="/5BC1F8BE" Ref="#GND?"  Part="1" 
AR Path="/5BA9E9B7/5BC1F8BE" Ref="#GND0105"  Part="1" 
F 0 "#GND0105" H 4250 2450 50  0001 C CNN
F 1 "GND" H 4150 2350 59  0000 L BNN
F 2 "" H 4250 2450 50  0001 C CNN
F 3 "" H 4250 2450 50  0001 C CNN
	1    4250 2450
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BC1F8C4
P 4050 2350
AR Path="/5BC1F8C4" Ref="#P+?"  Part="1" 
AR Path="/5BA9E9B7/5BC1F8C4" Ref="#P+0109"  Part="1" 
F 0 "#P+0109" H 4050 2350 50  0001 C CNN
F 1 "VCC" V 3950 2250 59  0000 L BNN
F 2 "" H 4050 2350 50  0001 C CNN
F 3 "" H 4050 2350 50  0001 C CNN
	1    4050 2350
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:+12V #P+?
U 1 0 5BC1F8CA
P 3750 1150
AR Path="/5BC1F8CA" Ref="#P+?"  Part="1" 
AR Path="/5BA9E9B7/5BC1F8CA" Ref="#P+0110"  Part="1" 
F 0 "#P+0110" H 3750 1150 50  0001 C CNN
F 1 "+12V" V 3650 950 59  0000 L BNN
F 2 "" H 3750 1150 50  0001 C CNN
F 3 "" H 3750 1150 50  0001 C CNN
	1    3750 1150
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BC1F8D0
P 1550 1150
AR Path="/5BC1F8D0" Ref="#GND?"  Part="1" 
AR Path="/5BA9E9B7/5BC1F8D0" Ref="#GND0106"  Part="1" 
F 0 "#GND0106" H 1550 1150 50  0001 C CNN
F 1 "GND" H 1450 1050 59  0000 L BNN
F 2 "" H 1550 1150 50  0001 C CNN
F 3 "" H 1550 1150 50  0001 C CNN
	1    1550 1150
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:PINHD-2X3 JP?
U 1 0 5BC1F8D6
P 1550 1750
AR Path="/5BC1F8D6" Ref="JP?"  Part="1" 
AR Path="/5BA9E9B7/5BC1F8D6" Ref="JP4"  Part="1" 
F 0 "JP4" H 1300 1975 59  0000 L BNN
F 1 "PINHD-2X3" H 1300 1450 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:2X03" H 1550 1750 50  0001 C CNN
F 3 "" H 1550 1750 50  0001 C CNN
	1    1550 1750
	-1   0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R?
U 1 0 5BC1F8DD
P 1850 1250
AR Path="/5BC1F8DD" Ref="R?"  Part="1" 
AR Path="/5BA9E9B7/5BC1F8DD" Ref="R5"  Part="1" 
F 0 "R5" H 1700 1309 59  0000 L BNN
F 1 "100k" H 1700 1120 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 1850 1250 50  0001 C CNN
F 3 "" H 1850 1250 50  0001 C CNN
	1    1850 1250
	0    -1   -1   0   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:PINHD-1X4 X-MOT?
U 1 0 5BC1F8E4
P 4150 1850
AR Path="/5BC1F8E4" Ref="X-MOT?"  Part="1" 
AR Path="/5BA9E9B7/5BC1F8E4" Ref="X-MOT1"  Part="1" 
F 0 "X-MOT1" H 3900 2075 59  0000 L BNN
F 1 "PINHD-1X4" H 3900 1450 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:1X04" H 4150 1850 50  0001 C CNN
F 3 "" H 4150 1850 50  0001 C CNN
	1    4150 1850
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BC1F8EB
P 1050 1350
AR Path="/5BC1F8EB" Ref="#P+?"  Part="1" 
AR Path="/5BA9E9B7/5BC1F8EB" Ref="#P+0111"  Part="1" 
F 0 "#P+0111" H 1050 1350 50  0001 C CNN
F 1 "VCC" V 950 1250 59  0000 L BNN
F 2 "" H 1050 1350 50  0001 C CNN
F 3 "" H 1050 1350 50  0001 C CNN
	1    1050 1350
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:A4988BREAKOUT U$?
U 1 0 5BC1F8F1
P 3150 1650
AR Path="/5BC1F8F1" Ref="U$?"  Part="1" 
AR Path="/5BA9E9B7/5BC1F8F1" Ref="U$6"  Part="1" 
F 0 "U$6" H 3150 1650 50  0001 C CNN
F 1 "A4988BREAKOUT" H 3150 1650 50  0001 C CNN
F 2 "RAMPS_1-41_eagle9:A4988BREAKOUT" H 3150 1650 50  0001 C CNN
F 3 "" H 3150 1650 50  0001 C CNN
	1    3150 1650
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R?
U 1 0 5BC1F8F7
P 2150 1250
AR Path="/5BC1F8F7" Ref="R?"  Part="1" 
AR Path="/5BA9E9B7/5BC1F8F7" Ref="R18"  Part="1" 
F 0 "R18" H 2000 1309 59  0000 L BNN
F 1 "10k" H 2000 1120 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 2150 1250 50  0001 C CNN
F 3 "" H 2150 1250 50  0001 C CNN
	1    2150 1250
	0    1    1    0   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BC1F8FE
P 2150 950
AR Path="/5BC1F8FE" Ref="#P+?"  Part="1" 
AR Path="/5BA9E9B7/5BC1F8FE" Ref="#P+0112"  Part="1" 
F 0 "#P+0112" H 2150 950 50  0001 C CNN
F 1 "VCC" V 2050 850 59  0000 L BNN
F 2 "" H 2150 950 50  0001 C CNN
F 3 "" H 2150 950 50  0001 C CNN
	1    2150 950 
	1    0    0    -1  
$EndComp
Text Notes 2650 1150 0    85   ~ 0
X
Wire Wire Line
	8500 2300 9000 2300
Wire Wire Line
	9000 2300 9000 2400
Wire Wire Line
	8500 1700 8600 1700
Wire Wire Line
	8600 1700 8600 1500
Wire Wire Line
	8600 1500 9000 1500
Wire Wire Line
	9000 1500 9000 2300
Connection ~ 9000 2300
Text Label 8500 2300 0    10   ~ 0
GND
Wire Wire Line
	6300 1100 6600 1100
Text Label 6300 1100 0    10   ~ 0
GND
Wire Wire Line
	8800 2500 8800 2600
Wire Wire Line
	8800 2600 8600 2600
Wire Wire Line
	8600 2600 8600 2200
Wire Wire Line
	8600 2200 8500 2200
Text Label 8800 2500 0    10   ~ 0
VCC
Wire Wire Line
	5800 1500 5800 1700
Wire Wire Line
	6100 1800 5800 1800
Wire Wire Line
	6100 1900 5800 1900
Wire Wire Line
	5800 1900 5800 1800
Wire Wire Line
	5800 1800 5800 1700
Wire Wire Line
	5800 1700 6100 1700
Connection ~ 5800 1800
Connection ~ 5800 1700
Text Label 5800 1500 0    10   ~ 0
VCC
Wire Wire Line
	8500 1300 8500 1600
Text Label 8500 1300 0    10   ~ 0
+12V
Wire Wire Line
	6900 1800 6400 1800
Wire Wire Line
	6900 2000 6900 2100
Wire Wire Line
	6900 1600 6700 1600
Wire Wire Line
	6900 1500 6900 1600
Connection ~ 6900 1600
Text Label 6700 1600 0    70   ~ 0
Y-EN
Wire Wire Line
	6900 2200 6700 2200
Text Label 6700 2200 0    70   ~ 0
Y-STEP
Wire Wire Line
	6900 2300 6700 2300
Text Label 6700 2300 0    70   ~ 0
Y-DIR
Wire Wire Line
	6900 1900 6400 1900
Wire Wire Line
	6900 1700 6600 1700
Wire Wire Line
	6600 1700 6400 1700
Wire Wire Line
	6600 1500 6600 1700
Connection ~ 6600 1700
Wire Wire Line
	8800 1800 8500 1800
Wire Wire Line
	8800 1900 8500 1900
Wire Wire Line
	8800 2000 8500 2000
Wire Wire Line
	8800 2100 8500 2100
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BC6E650
P 9000 2500
AR Path="/5BC6E650" Ref="#GND?"  Part="1" 
AR Path="/5BA9E9B7/5BC6E650" Ref="#GND0107"  Part="1" 
F 0 "#GND0107" H 9000 2500 50  0001 C CNN
F 1 "GND" H 8900 2400 59  0000 L BNN
F 2 "" H 9000 2500 50  0001 C CNN
F 3 "" H 9000 2500 50  0001 C CNN
	1    9000 2500
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BC6E656
P 8800 2400
AR Path="/5BC6E656" Ref="#P+?"  Part="1" 
AR Path="/5BA9E9B7/5BC6E656" Ref="#P+0113"  Part="1" 
F 0 "#P+0113" H 8800 2400 50  0001 C CNN
F 1 "VCC" V 8700 2300 59  0000 L BNN
F 2 "" H 8800 2400 50  0001 C CNN
F 3 "" H 8800 2400 50  0001 C CNN
	1    8800 2400
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:+12V #P+?
U 1 0 5BC6E65C
P 8500 1200
AR Path="/5BC6E65C" Ref="#P+?"  Part="1" 
AR Path="/5BA9E9B7/5BC6E65C" Ref="#P+0114"  Part="1" 
F 0 "#P+0114" H 8500 1200 50  0001 C CNN
F 1 "+12V" V 8400 1000 59  0000 L BNN
F 2 "" H 8500 1200 50  0001 C CNN
F 3 "" H 8500 1200 50  0001 C CNN
	1    8500 1200
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BC6E662
P 6300 1200
AR Path="/5BC6E662" Ref="#GND?"  Part="1" 
AR Path="/5BA9E9B7/5BC6E662" Ref="#GND0108"  Part="1" 
F 0 "#GND0108" H 6300 1200 50  0001 C CNN
F 1 "GND" H 6200 1100 59  0000 L BNN
F 2 "" H 6300 1200 50  0001 C CNN
F 3 "" H 6300 1200 50  0001 C CNN
	1    6300 1200
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:PINHD-2X3 JP?
U 1 0 5BC6E668
P 6300 1800
AR Path="/5BC6E668" Ref="JP?"  Part="1" 
AR Path="/5BA9E9B7/5BC6E668" Ref="JP5"  Part="1" 
F 0 "JP5" H 6050 2025 59  0000 L BNN
F 1 "PINHD-2X3" H 6050 1500 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:2X03" H 6300 1800 50  0001 C CNN
F 3 "" H 6300 1800 50  0001 C CNN
	1    6300 1800
	-1   0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R?
U 1 0 5BC6E66F
P 6600 1300
AR Path="/5BC6E66F" Ref="R?"  Part="1" 
AR Path="/5BA9E9B7/5BC6E66F" Ref="R8"  Part="1" 
F 0 "R8" H 6450 1359 59  0000 L BNN
F 1 "100k" H 6450 1170 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 6600 1300 50  0001 C CNN
F 3 "" H 6600 1300 50  0001 C CNN
	1    6600 1300
	0    -1   -1   0   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:PINHD-1X4 Y-MOT?
U 1 0 5BC6E676
P 8900 1900
AR Path="/5BC6E676" Ref="Y-MOT?"  Part="1" 
AR Path="/5BA9E9B7/5BC6E676" Ref="Y-MOT1"  Part="1" 
F 0 "Y-MOT1" H 8650 2125 59  0000 L BNN
F 1 "PINHD-1X4" H 8650 1500 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:1X04" H 8900 1900 50  0001 C CNN
F 3 "" H 8900 1900 50  0001 C CNN
	1    8900 1900
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BC6E67D
P 5800 1400
AR Path="/5BC6E67D" Ref="#P+?"  Part="1" 
AR Path="/5BA9E9B7/5BC6E67D" Ref="#P+0115"  Part="1" 
F 0 "#P+0115" H 5800 1400 50  0001 C CNN
F 1 "VCC" V 5700 1300 59  0000 L BNN
F 2 "" H 5800 1400 50  0001 C CNN
F 3 "" H 5800 1400 50  0001 C CNN
	1    5800 1400
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:A4988BREAKOUT U$?
U 1 0 5BC6E683
P 7900 1700
AR Path="/5BC6E683" Ref="U$?"  Part="1" 
AR Path="/5BA9E9B7/5BC6E683" Ref="U$7"  Part="1" 
F 0 "U$7" H 7900 1700 50  0001 C CNN
F 1 "A4988BREAKOUT" H 7900 1700 50  0001 C CNN
F 2 "RAMPS_1-41_eagle9:A4988BREAKOUT" H 7900 1700 50  0001 C CNN
F 3 "" H 7900 1700 50  0001 C CNN
	1    7900 1700
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R?
U 1 0 5BC6E689
P 6900 1300
AR Path="/5BC6E689" Ref="R?"  Part="1" 
AR Path="/5BA9E9B7/5BC6E689" Ref="R19"  Part="1" 
F 0 "R19" H 6750 1359 59  0000 L BNN
F 1 "10k" H 6750 1170 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 6900 1300 50  0001 C CNN
F 3 "" H 6900 1300 50  0001 C CNN
	1    6900 1300
	0    1    1    0   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BC6E690
P 6900 1000
AR Path="/5BC6E690" Ref="#P+?"  Part="1" 
AR Path="/5BA9E9B7/5BC6E690" Ref="#P+0116"  Part="1" 
F 0 "#P+0116" H 6900 1000 50  0001 C CNN
F 1 "VCC" V 6800 900 59  0000 L BNN
F 2 "" H 6900 1000 50  0001 C CNN
F 3 "" H 6900 1000 50  0001 C CNN
	1    6900 1000
	1    0    0    -1  
$EndComp
Text Notes 7300 1200 0    85   ~ 0
Y
Wire Wire Line
	3750 4050 4450 4050
Wire Wire Line
	4450 4050 4450 4150
Wire Wire Line
	3750 3450 3850 3450
Wire Wire Line
	3850 3450 3850 3250
Wire Wire Line
	3850 3250 4750 3250
Wire Wire Line
	4750 3250 4750 4050
Wire Wire Line
	4750 4050 4450 4050
Connection ~ 4450 4050
Text Label 3750 4050 0    10   ~ 0
GND
Wire Wire Line
	1550 2850 1850 2850
Text Label 1550 2850 0    10   ~ 0
GND
Wire Wire Line
	4050 4250 4050 4350
Wire Wire Line
	4050 4350 3850 4350
Wire Wire Line
	3850 4350 3850 3950
Wire Wire Line
	3850 3950 3750 3950
Text Label 4050 4250 0    10   ~ 0
VCC
Wire Wire Line
	1050 3250 1050 3450
Wire Wire Line
	1350 3550 1050 3550
Wire Wire Line
	1350 3650 1050 3650
Wire Wire Line
	1050 3650 1050 3550
Wire Wire Line
	1050 3550 1050 3450
Wire Wire Line
	1050 3450 1350 3450
Connection ~ 1050 3550
Connection ~ 1050 3450
Text Label 1050 3250 0    10   ~ 0
VCC
Wire Wire Line
	3750 3050 3750 3350
Text Label 3750 3050 0    10   ~ 0
+12V
Wire Wire Line
	2150 3550 1650 3550
Wire Wire Line
	2150 3750 2150 3850
Wire Wire Line
	2150 3650 1650 3650
Wire Wire Line
	2150 3450 1850 3450
Wire Wire Line
	1850 3450 1650 3450
Wire Wire Line
	1850 3250 1850 3450
Connection ~ 1850 3450
Wire Wire Line
	4050 3550 3750 3550
Wire Wire Line
	4050 3550 4550 3550
Connection ~ 4050 3550
Wire Wire Line
	4050 3650 3750 3650
Wire Wire Line
	4050 3650 4550 3650
Connection ~ 4050 3650
Wire Wire Line
	4050 3750 3750 3750
Wire Wire Line
	4050 3750 4550 3750
Connection ~ 4050 3750
Wire Wire Line
	4050 3850 3750 3850
Wire Wire Line
	4050 3850 4550 3850
Connection ~ 4050 3850
Wire Wire Line
	2150 3350 1950 3350
Wire Wire Line
	2150 3250 2150 3350
Connection ~ 2150 3350
Text Label 1950 3350 0    70   ~ 0
Z-EN
Wire Wire Line
	2150 3950 1950 3950
Text Label 1950 3950 0    70   ~ 0
Z-STEP
Wire Wire Line
	2150 4050 1950 4050
Text Label 1950 4050 0    70   ~ 0
Z-DIR
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BCAFC85
P 4450 4250
AR Path="/5BCAFC85" Ref="#GND?"  Part="1" 
AR Path="/5BA9E9B7/5BCAFC85" Ref="#GND0109"  Part="1" 
F 0 "#GND0109" H 4450 4250 50  0001 C CNN
F 1 "GND" H 4350 4150 59  0000 L BNN
F 2 "" H 4450 4250 50  0001 C CNN
F 3 "" H 4450 4250 50  0001 C CNN
	1    4450 4250
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BCAFC8B
P 4050 4150
AR Path="/5BCAFC8B" Ref="#P+?"  Part="1" 
AR Path="/5BA9E9B7/5BCAFC8B" Ref="#P+0117"  Part="1" 
F 0 "#P+0117" H 4050 4150 50  0001 C CNN
F 1 "VCC" V 3950 4050 59  0000 L BNN
F 2 "" H 4050 4150 50  0001 C CNN
F 3 "" H 4050 4150 50  0001 C CNN
	1    4050 4150
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:+12V #P+?
U 1 0 5BCAFC91
P 3750 2950
AR Path="/5BCAFC91" Ref="#P+?"  Part="1" 
AR Path="/5BA9E9B7/5BCAFC91" Ref="#P+0118"  Part="1" 
F 0 "#P+0118" H 3750 2950 50  0001 C CNN
F 1 "+12V" V 3650 2750 59  0000 L BNN
F 2 "" H 3750 2950 50  0001 C CNN
F 3 "" H 3750 2950 50  0001 C CNN
	1    3750 2950
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BCAFC97
P 1550 2950
AR Path="/5BCAFC97" Ref="#GND?"  Part="1" 
AR Path="/5BA9E9B7/5BCAFC97" Ref="#GND0110"  Part="1" 
F 0 "#GND0110" H 1550 2950 50  0001 C CNN
F 1 "GND" H 1450 2850 59  0000 L BNN
F 2 "" H 1550 2950 50  0001 C CNN
F 3 "" H 1550 2950 50  0001 C CNN
	1    1550 2950
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:PINHD-2X3 JP?
U 1 0 5BCAFC9D
P 1550 3550
AR Path="/5BCAFC9D" Ref="JP?"  Part="1" 
AR Path="/5BA9E9B7/5BCAFC9D" Ref="JP6"  Part="1" 
F 0 "JP6" H 1300 3775 59  0000 L BNN
F 1 "PINHD-2X3" H 1300 3250 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:2X03" H 1550 3550 50  0001 C CNN
F 3 "" H 1550 3550 50  0001 C CNN
	1    1550 3550
	-1   0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R?
U 1 0 5BCAFCA4
P 1850 3050
AR Path="/5BCAFCA4" Ref="R?"  Part="1" 
AR Path="/5BA9E9B7/5BCAFCA4" Ref="R10"  Part="1" 
F 0 "R10" H 1700 3109 59  0000 L BNN
F 1 "100k" H 1700 2920 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 1850 3050 50  0001 C CNN
F 3 "" H 1850 3050 50  0001 C CNN
	1    1850 3050
	0    -1   -1   0   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:PINHD-1X4 Z-MOT?
U 1 0 5BCAFCAB
P 4650 3650
AR Path="/5BCAFCAB" Ref="Z-MOT?"  Part="1" 
AR Path="/5BA9E9B7/5BCAFCAB" Ref="Z-MOT2"  Part="1" 
F 0 "Z-MOT2" H 4400 3875 59  0000 L BNN
F 1 "PINHD-1X4" H 4400 3250 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:1X04" H 4650 3650 50  0001 C CNN
F 3 "" H 4650 3650 50  0001 C CNN
	1    4650 3650
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BCAFCB2
P 1050 3150
AR Path="/5BCAFCB2" Ref="#P+?"  Part="1" 
AR Path="/5BA9E9B7/5BCAFCB2" Ref="#P+0119"  Part="1" 
F 0 "#P+0119" H 1050 3150 50  0001 C CNN
F 1 "VCC" V 950 3050 59  0000 L BNN
F 2 "" H 1050 3150 50  0001 C CNN
F 3 "" H 1050 3150 50  0001 C CNN
	1    1050 3150
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:A4988BREAKOUT U$?
U 1 0 5BCAFCB8
P 3150 3450
AR Path="/5BCAFCB8" Ref="U$?"  Part="1" 
AR Path="/5BA9E9B7/5BCAFCB8" Ref="U$8"  Part="1" 
F 0 "U$8" H 3150 3450 50  0001 C CNN
F 1 "A4988BREAKOUT" H 3150 3450 50  0001 C CNN
F 2 "RAMPS_1-41_eagle9:A4988BREAKOUT" H 3150 3450 50  0001 C CNN
F 3 "" H 3150 3450 50  0001 C CNN
	1    3150 3450
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:PINHD-1X4 Z-MOT?
U 1 0 5BCAFCBE
P 4150 3650
AR Path="/5BCAFCBE" Ref="Z-MOT?"  Part="1" 
AR Path="/5BA9E9B7/5BCAFCBE" Ref="Z-MOT1"  Part="1" 
F 0 "Z-MOT1" H 3900 3875 59  0000 L BNN
F 1 "PINHD-1X4" H 3900 3250 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:1X04" H 4150 3650 50  0001 C CNN
F 3 "" H 4150 3650 50  0001 C CNN
	1    4150 3650
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R?
U 1 0 5BCAFCC5
P 2150 3050
AR Path="/5BCAFCC5" Ref="R?"  Part="1" 
AR Path="/5BA9E9B7/5BCAFCC5" Ref="R20"  Part="1" 
F 0 "R20" H 2000 3109 59  0000 L BNN
F 1 "10k" H 2000 2920 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 2150 3050 50  0001 C CNN
F 3 "" H 2150 3050 50  0001 C CNN
	1    2150 3050
	0    1    1    0   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BCAFCCC
P 2150 2750
AR Path="/5BCAFCCC" Ref="#P+?"  Part="1" 
AR Path="/5BA9E9B7/5BCAFCCC" Ref="#P+0120"  Part="1" 
F 0 "#P+0120" H 2150 2750 50  0001 C CNN
F 1 "VCC" V 2050 2650 59  0000 L BNN
F 2 "" H 2150 2750 50  0001 C CNN
F 3 "" H 2150 2750 50  0001 C CNN
	1    2150 2750
	1    0    0    -1  
$EndComp
Text Notes 2550 2950 0    85   ~ 0
Z
$Comp
L RAMPS_1-41_eagle9-eagle-import:CPOL-US153CLV-0605 C?
U 1 0 5BCD6C63
P 6550 5550
AR Path="/5BCD6C63" Ref="C?"  Part="1" 
AR Path="/5BA9E9B7/5BCD6C63" Ref="C3"  Part="1" 
F 0 "C3" H 6590 5575 59  0000 L BNN
F 1 "100uF" H 6590 5385 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:153CLV-0605" H 6550 5550 50  0001 C CNN
F 3 "" H 6550 5550 50  0001 C CNN
	1    6550 5550
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:CPOL-US153CLV-0605 C?
U 1 0 5BCD6C6A
P 7150 5550
AR Path="/5BCD6C6A" Ref="C?"  Part="1" 
AR Path="/5BA9E9B7/5BCD6C6A" Ref="C4"  Part="1" 
F 0 "C4" H 7190 5575 59  0000 L BNN
F 1 "100uF" H 7190 5385 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:153CLV-0605" H 7150 5550 50  0001 C CNN
F 3 "" H 7150 5550 50  0001 C CNN
	1    7150 5550
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:CPOL-US153CLV-0605 C?
U 1 0 5BCD6C71
P 7750 5550
AR Path="/5BCD6C71" Ref="C?"  Part="1" 
AR Path="/5BA9E9B7/5BCD6C71" Ref="C7"  Part="1" 
F 0 "C7" H 7790 5575 59  0000 L BNN
F 1 "100uF" H 7790 5385 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:153CLV-0605" H 7750 5550 50  0001 C CNN
F 3 "" H 7750 5550 50  0001 C CNN
	1    7750 5550
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:CPOL-US153CLV-0605 C?
U 1 0 5BCD6C78
P 8350 5550
AR Path="/5BCD6C78" Ref="C?"  Part="1" 
AR Path="/5BA9E9B7/5BCD6C78" Ref="C9"  Part="1" 
F 0 "C9" H 8390 5575 59  0000 L BNN
F 1 "100uF" H 8390 5385 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:153CLV-0605" H 8350 5550 50  0001 C CNN
F 3 "" H 8350 5550 50  0001 C CNN
	1    8350 5550
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:CPOL-US153CLV-0605 C?
U 1 0 5BCD6C7F
P 8950 5550
AR Path="/5BCD6C7F" Ref="C?"  Part="1" 
AR Path="/5BA9E9B7/5BCD6C7F" Ref="C10"  Part="1" 
F 0 "C10" H 8990 5575 59  0000 L BNN
F 1 "100uF" H 8990 5385 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:153CLV-0605" H 8950 5550 50  0001 C CNN
F 3 "" H 8950 5550 50  0001 C CNN
	1    8950 5550
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:+12V #P+?
U 1 0 5BCD6C86
P 6550 5350
AR Path="/5BCD6C86" Ref="#P+?"  Part="1" 
AR Path="/5BA9E9B7/5BCD6C86" Ref="#P+0121"  Part="1" 
F 0 "#P+0121" H 6550 5350 50  0001 C CNN
F 1 "+12V" V 6450 5150 59  0000 L BNN
F 2 "" H 6550 5350 50  0001 C CNN
F 3 "" H 6550 5350 50  0001 C CNN
	1    6550 5350
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:+12V #P+?
U 1 0 5BCD6C8C
P 7150 5350
AR Path="/5BCD6C8C" Ref="#P+?"  Part="1" 
AR Path="/5BA9E9B7/5BCD6C8C" Ref="#P+0122"  Part="1" 
F 0 "#P+0122" H 7150 5350 50  0001 C CNN
F 1 "+12V" V 7050 5150 59  0000 L BNN
F 2 "" H 7150 5350 50  0001 C CNN
F 3 "" H 7150 5350 50  0001 C CNN
	1    7150 5350
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:+12V #P+?
U 1 0 5BCD6C92
P 7750 5350
AR Path="/5BCD6C92" Ref="#P+?"  Part="1" 
AR Path="/5BA9E9B7/5BCD6C92" Ref="#P+0123"  Part="1" 
F 0 "#P+0123" H 7750 5350 50  0001 C CNN
F 1 "+12V" V 7650 5150 59  0000 L BNN
F 2 "" H 7750 5350 50  0001 C CNN
F 3 "" H 7750 5350 50  0001 C CNN
	1    7750 5350
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:+12V #P+?
U 1 0 5BCD6C98
P 8350 5350
AR Path="/5BCD6C98" Ref="#P+?"  Part="1" 
AR Path="/5BA9E9B7/5BCD6C98" Ref="#P+0124"  Part="1" 
F 0 "#P+0124" H 8350 5350 50  0001 C CNN
F 1 "+12V" V 8250 5150 59  0000 L BNN
F 2 "" H 8350 5350 50  0001 C CNN
F 3 "" H 8350 5350 50  0001 C CNN
	1    8350 5350
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:+12V #P+?
U 1 0 5BCD6C9E
P 8950 5350
AR Path="/5BCD6C9E" Ref="#P+?"  Part="1" 
AR Path="/5BA9E9B7/5BCD6C9E" Ref="#P+0125"  Part="1" 
F 0 "#P+0125" H 8950 5350 50  0001 C CNN
F 1 "+12V" V 8850 5150 59  0000 L BNN
F 2 "" H 8950 5350 50  0001 C CNN
F 3 "" H 8950 5350 50  0001 C CNN
	1    8950 5350
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BCD6CA4
P 6550 5850
AR Path="/5BCD6CA4" Ref="#GND?"  Part="1" 
AR Path="/5BA9E9B7/5BCD6CA4" Ref="#GND0111"  Part="1" 
F 0 "#GND0111" H 6550 5850 50  0001 C CNN
F 1 "GND" H 6450 5750 59  0000 L BNN
F 2 "" H 6550 5850 50  0001 C CNN
F 3 "" H 6550 5850 50  0001 C CNN
	1    6550 5850
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BCD6CAA
P 7150 5850
AR Path="/5BCD6CAA" Ref="#GND?"  Part="1" 
AR Path="/5BA9E9B7/5BCD6CAA" Ref="#GND0112"  Part="1" 
F 0 "#GND0112" H 7150 5850 50  0001 C CNN
F 1 "GND" H 7050 5750 59  0000 L BNN
F 2 "" H 7150 5850 50  0001 C CNN
F 3 "" H 7150 5850 50  0001 C CNN
	1    7150 5850
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BCD6CB0
P 7750 5850
AR Path="/5BCD6CB0" Ref="#GND?"  Part="1" 
AR Path="/5BA9E9B7/5BCD6CB0" Ref="#GND0113"  Part="1" 
F 0 "#GND0113" H 7750 5850 50  0001 C CNN
F 1 "GND" H 7650 5750 59  0000 L BNN
F 2 "" H 7750 5850 50  0001 C CNN
F 3 "" H 7750 5850 50  0001 C CNN
	1    7750 5850
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BCD6CB6
P 8350 5850
AR Path="/5BCD6CB6" Ref="#GND?"  Part="1" 
AR Path="/5BA9E9B7/5BCD6CB6" Ref="#GND0114"  Part="1" 
F 0 "#GND0114" H 8350 5850 50  0001 C CNN
F 1 "GND" H 8250 5750 59  0000 L BNN
F 2 "" H 8350 5850 50  0001 C CNN
F 3 "" H 8350 5850 50  0001 C CNN
	1    8350 5850
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BCD6CBC
P 8950 5850
AR Path="/5BCD6CBC" Ref="#GND?"  Part="1" 
AR Path="/5BA9E9B7/5BCD6CBC" Ref="#GND0115"  Part="1" 
F 0 "#GND0115" H 8950 5850 50  0001 C CNN
F 1 "GND" H 8850 5750 59  0000 L BNN
F 2 "" H 8950 5850 50  0001 C CNN
F 3 "" H 8950 5850 50  0001 C CNN
	1    8950 5850
	1    0    0    -1  
$EndComp
$EndSCHEMATC
