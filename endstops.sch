EESchema Schematic File Version 4
LIBS:RAMPS_1-41-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 4 6
Title "Reprap Arduino Mega Pololu Shield"
Date "2018-08-21"
Rev "1.4"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5250 2600 5050 2600
Wire Wire Line
	5050 2600 4450 2600
Wire Wire Line
	4450 2600 3850 2600
Wire Wire Line
	3850 2600 3250 2600
Wire Wire Line
	3250 2600 2650 2600
Wire Wire Line
	2650 2600 2050 2600
Wire Wire Line
	5050 2200 5050 2600
Wire Wire Line
	4450 2200 4450 2600
Wire Wire Line
	3850 2200 3850 2600
Wire Wire Line
	3250 2200 3250 2600
Wire Wire Line
	2650 2200 2650 2600
Wire Wire Line
	2050 2200 2050 2600
Connection ~ 4450 2600
Connection ~ 5050 2600
Connection ~ 3850 2600
Connection ~ 3250 2600
Connection ~ 2650 2600
Text Label 5250 2600 0    10   ~ 0
GND
Wire Wire Line
	1750 2400 2150 2400
Wire Wire Line
	2150 2400 2750 2400
Wire Wire Line
	2750 2400 3350 2400
Wire Wire Line
	3350 2400 3950 2400
Wire Wire Line
	3950 2400 4550 2400
Wire Wire Line
	4550 2400 5150 2400
Wire Wire Line
	5150 2400 5150 2200
Wire Wire Line
	4550 2200 4550 2400
Wire Wire Line
	3950 2200 3950 2400
Wire Wire Line
	3350 2200 3350 2400
Wire Wire Line
	2750 2200 2750 2400
Wire Wire Line
	2150 2200 2150 2400
Connection ~ 4550 2400
Connection ~ 3950 2400
Connection ~ 3350 2400
Connection ~ 2750 2400
Connection ~ 2150 2400
Text Label 1750 2400 0    10   ~ 0
VCC
Wire Wire Line
	1950 2200 1950 3100
Text Label 1950 3100 1    70   ~ 0
X-MIN
Wire Wire Line
	3150 2200 3150 3100
Text Label 3150 3100 1    70   ~ 0
Y-MIN
Wire Wire Line
	4350 2200 4350 3100
Text Label 4350 3100 1    70   ~ 0
Z-MIN
Wire Wire Line
	2550 2200 2550 3100
Text Label 2550 3100 1    70   ~ 0
X-MAX
Wire Wire Line
	3750 2200 3750 3100
Text Label 3750 3100 1    70   ~ 0
Y-MAX
Wire Wire Line
	4950 2200 4950 3100
Text Label 4950 3100 1    70   ~ 0
Z-MAX
$Comp
L RAMPS_1-41_eagle9-eagle-import:3PIN-HEADER Y-?
U 1 0 5BD89BEF
P 3250 1900
AR Path="/5BD89BEF" Ref="Y-?"  Part="1" 
AR Path="/5BD77412/5BD89BEF" Ref="Y-1"  Part="1" 
F 0 "Y-1" H 3200 2130 59  0000 L BNN
F 1 "3PIN-HEADER" H 3200 1600 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:3PIN-HEADER" H 3250 1900 50  0001 C CNN
F 3 "" H 3250 1900 50  0001 C CNN
	1    3250 1900
	0    1    1    0   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:3PIN-HEADER X-?
U 1 0 5BD89BF6
P 2050 1900
AR Path="/5BD89BF6" Ref="X-?"  Part="1" 
AR Path="/5BD77412/5BD89BF6" Ref="X-1"  Part="1" 
F 0 "X-1" H 2000 2130 59  0000 L BNN
F 1 "3PIN-HEADER" H 2000 1600 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:3PIN-HEADER" H 2050 1900 50  0001 C CNN
F 3 "" H 2050 1900 50  0001 C CNN
	1    2050 1900
	0    1    1    0   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:3PIN-HEADER X+?
U 1 0 5BD89BFD
P 2650 1900
AR Path="/5BD89BFD" Ref="X+?"  Part="1" 
AR Path="/5BD77412/5BD89BFD" Ref="X+1"  Part="1" 
F 0 "X+1" H 2600 2130 59  0000 L BNN
F 1 "3PIN-HEADER" H 2600 1600 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:3PIN-HEADER" H 2650 1900 50  0001 C CNN
F 3 "" H 2650 1900 50  0001 C CNN
	1    2650 1900
	0    1    1    0   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BD89C04
P 1750 2300
AR Path="/5BD89C04" Ref="#P+?"  Part="1" 
AR Path="/5BD77412/5BD89C04" Ref="#P+0128"  Part="1" 
F 0 "#P+0128" H 1750 2300 50  0001 C CNN
F 1 "VCC" V 1650 2200 59  0000 L BNN
F 2 "" H 1750 2300 50  0001 C CNN
F 3 "" H 1750 2300 50  0001 C CNN
	1    1750 2300
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BD89C0A
P 5250 2700
AR Path="/5BD89C0A" Ref="#GND?"  Part="1" 
AR Path="/5BD77412/5BD89C0A" Ref="#GND0119"  Part="1" 
F 0 "#GND0119" H 5250 2700 50  0001 C CNN
F 1 "GND" H 5150 2600 59  0000 L BNN
F 2 "" H 5250 2700 50  0001 C CNN
F 3 "" H 5250 2700 50  0001 C CNN
	1    5250 2700
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:3PIN-HEADER Y+?
U 1 0 5BD89C10
P 3850 1900
AR Path="/5BD89C10" Ref="Y+?"  Part="1" 
AR Path="/5BD77412/5BD89C10" Ref="Y+1"  Part="1" 
F 0 "Y+1" H 3800 2130 59  0000 L BNN
F 1 "3PIN-HEADER" H 3800 1600 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:3PIN-HEADER" H 3850 1900 50  0001 C CNN
F 3 "" H 3850 1900 50  0001 C CNN
	1    3850 1900
	0    1    1    0   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:3PIN-HEADER Z-?
U 1 0 5BD89C17
P 4450 1900
AR Path="/5BD89C17" Ref="Z-?"  Part="1" 
AR Path="/5BD77412/5BD89C17" Ref="Z-1"  Part="1" 
F 0 "Z-1" H 4400 2130 59  0000 L BNN
F 1 "3PIN-HEADER" H 4400 1600 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:3PIN-HEADER" H 4450 1900 50  0001 C CNN
F 3 "" H 4450 1900 50  0001 C CNN
	1    4450 1900
	0    1    1    0   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:3PIN-HEADER Z+?
U 1 0 5BD89C1E
P 5050 1900
AR Path="/5BD89C1E" Ref="Z+?"  Part="1" 
AR Path="/5BD77412/5BD89C1E" Ref="Z+1"  Part="1" 
F 0 "Z+1" H 5000 2130 59  0000 L BNN
F 1 "3PIN-HEADER" H 5000 1600 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:3PIN-HEADER" H 5050 1900 50  0001 C CNN
F 3 "" H 5050 1900 50  0001 C CNN
	1    5050 1900
	0    1    1    0   
$EndComp
Text Notes 2950 1550 0    170  ~ 0
Endstops
Wire Wire Line
	2550 5800 2350 5800
Wire Wire Line
	2350 5800 2350 6000
Wire Wire Line
	2350 4600 2350 5800
Wire Wire Line
	3350 4600 2350 4600
Connection ~ 2350 5800
Text Label 2550 5800 0    10   ~ 0
GND
Wire Wire Line
	3450 5900 3150 5900
Wire Wire Line
	3150 5900 3150 4800
Wire Wire Line
	3150 4800 3550 4800
Wire Wire Line
	3150 6000 3150 5900
Wire Wire Line
	3550 4800 3550 4600
Connection ~ 3150 5900
Text Label 3450 5900 0    10   ~ 0
GND
Wire Wire Line
	4050 6000 4050 5900
Wire Wire Line
	4050 5900 4050 4700
Wire Wire Line
	4050 4700 3750 4700
Wire Wire Line
	3750 4700 3750 4600
Wire Wire Line
	4250 5900 4050 5900
Connection ~ 4050 5900
Text Label 4050 6000 0    10   ~ 0
GND
Wire Wire Line
	2550 5400 2550 5500
Wire Wire Line
	2850 5400 2550 5400
Wire Wire Line
	2850 5400 2850 6300
Wire Wire Line
	2850 5400 2850 4700
Wire Wire Line
	2850 4700 3450 4700
Wire Wire Line
	3450 4700 3450 4600
Connection ~ 2550 5400
Connection ~ 2850 5400
Text Label 2850 6300 1    70   ~ 0
THERM0
Wire Wire Line
	3450 5400 3450 5600
Wire Wire Line
	3450 5400 3750 5400
Wire Wire Line
	3750 5400 3750 4800
Wire Wire Line
	3750 5400 3750 6200
Wire Wire Line
	3750 4800 3650 4800
Wire Wire Line
	3650 4800 3650 4600
Connection ~ 3450 5400
Connection ~ 3750 5400
Text Label 3750 6200 1    70   ~ 0
THERM1
Wire Wire Line
	4750 6200 4750 5400
Wire Wire Line
	4750 5400 4750 4600
Wire Wire Line
	4750 4600 3850 4600
Wire Wire Line
	4250 5600 4250 5400
Wire Wire Line
	4750 5400 4250 5400
Connection ~ 4250 5400
Connection ~ 4750 5400
Text Label 4750 6200 1    70   ~ 0
THERM2
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R?
U 1 0 5BE60AD9
P 3450 5200
AR Path="/5BE60AD9" Ref="R?"  Part="1" 
AR Path="/5BD9CED8/5BE60AD9" Ref="R?"  Part="1" 
AR Path="/5BD77412/5BE60AD9" Ref="R1"  Part="1" 
F 0 "R1" H 3300 5259 59  0000 L BNN
F 1 "4.7K" H 3300 5070 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 3450 5200 50  0001 C CNN
F 3 "" H 3450 5200 50  0001 C CNN
	1    3450 5200
	0    -1   -1   0   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:CPOL-US153CLV-0405 C?
U 1 0 5BE60AE0
P 3450 5700
AR Path="/5BE60AE0" Ref="C?"  Part="1" 
AR Path="/5BD9CED8/5BE60AE0" Ref="C?"  Part="1" 
AR Path="/5BD77412/5BE60AE0" Ref="C8"  Part="1" 
F 0 "C8" H 3490 5725 59  0000 L BNN
F 1 "10uF" H 3490 5535 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:153CLV-0405" H 3450 5700 50  0001 C CNN
F 3 "" H 3450 5700 50  0001 C CNN
	1    3450 5700
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BE60AE7
P 3150 6100
AR Path="/5BD9CED8/5BE60AE7" Ref="#GND?"  Part="1" 
AR Path="/5BD77412/5BE60AE7" Ref="#GND0120"  Part="1" 
F 0 "#GND0120" H 3150 6100 50  0001 C CNN
F 1 "GND" H 3050 6000 59  0000 L BNN
F 2 "" H 3150 6100 50  0001 C CNN
F 3 "" H 3150 6100 50  0001 C CNN
	1    3150 6100
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BE60AED
P 3450 4900
AR Path="/5BD9CED8/5BE60AED" Ref="#P+?"  Part="1" 
AR Path="/5BD77412/5BE60AED" Ref="#P+0129"  Part="1" 
F 0 "#P+0129" H 3450 4900 50  0001 C CNN
F 1 "VCC" V 3350 4800 59  0000 L BNN
F 2 "" H 3450 4900 50  0001 C CNN
F 3 "" H 3450 4900 50  0001 C CNN
	1    3450 4900
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BE60AF3
P 2550 4900
AR Path="/5BD9CED8/5BE60AF3" Ref="#P+?"  Part="1" 
AR Path="/5BD77412/5BE60AF3" Ref="#P+0130"  Part="1" 
F 0 "#P+0130" H 2550 4900 50  0001 C CNN
F 1 "VCC" V 2450 4800 59  0000 L BNN
F 2 "" H 2550 4900 50  0001 C CNN
F 3 "" H 2550 4900 50  0001 C CNN
	1    2550 4900
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R?
U 1 0 5BE60AF9
P 2550 5200
AR Path="/5BE60AF9" Ref="R?"  Part="1" 
AR Path="/5BD9CED8/5BE60AF9" Ref="R?"  Part="1" 
AR Path="/5BD77412/5BE60AF9" Ref="R7"  Part="1" 
F 0 "R7" H 2400 5259 59  0000 L BNN
F 1 "4.7K" H 2400 5070 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 2550 5200 50  0001 C CNN
F 3 "" H 2550 5200 50  0001 C CNN
	1    2550 5200
	0    -1   -1   0   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:CPOL-US153CLV-0405 C?
U 1 0 5BE60B00
P 2550 5600
AR Path="/5BE60B00" Ref="C?"  Part="1" 
AR Path="/5BD9CED8/5BE60B00" Ref="C?"  Part="1" 
AR Path="/5BD77412/5BE60B00" Ref="C5"  Part="1" 
F 0 "C5" H 2590 5625 59  0000 L BNN
F 1 "10uF" H 2590 5435 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:153CLV-0405" H 2550 5600 50  0001 C CNN
F 3 "" H 2550 5600 50  0001 C CNN
	1    2550 5600
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BE60B07
P 2350 6100
AR Path="/5BD9CED8/5BE60B07" Ref="#GND?"  Part="1" 
AR Path="/5BD77412/5BE60B07" Ref="#GND0121"  Part="1" 
F 0 "#GND0121" H 2350 6100 50  0001 C CNN
F 1 "GND" H 2250 6000 59  0000 L BNN
F 2 "" H 2350 6100 50  0001 C CNN
F 3 "" H 2350 6100 50  0001 C CNN
	1    2350 6100
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:M06SIP JP?
U 1 0 5BE60B0D
P 3550 4400
AR Path="/5BE60B0D" Ref="JP?"  Part="1" 
AR Path="/5BD9CED8/5BE60B0D" Ref="JP?"  Part="1" 
AR Path="/5BD77412/5BE60B0D" Ref="JP7"  Part="1" 
F 0 "JP7" H 3350 4830 59  0000 L BNN
F 1 "M06SIP" H 3350 4000 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:1X06" H 3550 4400 50  0001 C CNN
F 3 "" H 3550 4400 50  0001 C CNN
	1    3550 4400
	0    1    1    0   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R?
U 1 0 5BE60B14
P 4250 5200
AR Path="/5BE60B14" Ref="R?"  Part="1" 
AR Path="/5BD9CED8/5BE60B14" Ref="R?"  Part="1" 
AR Path="/5BD77412/5BE60B14" Ref="R11"  Part="1" 
F 0 "R11" H 4100 5259 59  0000 L BNN
F 1 "4.7K" H 4100 5070 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 4250 5200 50  0001 C CNN
F 3 "" H 4250 5200 50  0001 C CNN
	1    4250 5200
	0    -1   -1   0   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:CPOL-US153CLV-0405 C?
U 1 0 5BE60B1B
P 4250 5700
AR Path="/5BE60B1B" Ref="C?"  Part="1" 
AR Path="/5BD9CED8/5BE60B1B" Ref="C?"  Part="1" 
AR Path="/5BD77412/5BE60B1B" Ref="C1"  Part="1" 
F 0 "C1" H 4290 5725 59  0000 L BNN
F 1 "10uF" H 4290 5535 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:153CLV-0405" H 4250 5700 50  0001 C CNN
F 3 "" H 4250 5700 50  0001 C CNN
	1    4250 5700
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BE60B22
P 4050 6100
AR Path="/5BD9CED8/5BE60B22" Ref="#GND?"  Part="1" 
AR Path="/5BD77412/5BE60B22" Ref="#GND0122"  Part="1" 
F 0 "#GND0122" H 4050 6100 50  0001 C CNN
F 1 "GND" H 3950 6000 59  0000 L BNN
F 2 "" H 4050 6100 50  0001 C CNN
F 3 "" H 4050 6100 50  0001 C CNN
	1    4050 6100
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BE60B28
P 4250 4900
AR Path="/5BD9CED8/5BE60B28" Ref="#P+?"  Part="1" 
AR Path="/5BD77412/5BE60B28" Ref="#P+0131"  Part="1" 
F 0 "#P+0131" H 4250 4900 50  0001 C CNN
F 1 "VCC" V 4150 4800 59  0000 L BNN
F 2 "" H 4250 4900 50  0001 C CNN
F 3 "" H 4250 4900 50  0001 C CNN
	1    4250 4900
	1    0    0    -1  
$EndComp
Text Notes 2250 5800 0    85   ~ 0
Thermistor 0
Text Notes 3050 5800 0    85   ~ 0
Thermistor 1
Text Notes 3950 5800 0    85   ~ 0
Thermistor 2
Text Notes 2800 4000 0    170  ~ 0
Thermistors
Text Notes 2800 4000 0    170  ~ 0
Thermistors
Wire Wire Line
	8450 4700 8450 4500
Wire Wire Line
	8450 4500 8250 4500
Text Label 8450 4700 0    10   ~ 0
GND
Wire Wire Line
	8450 4000 8450 3700
Wire Wire Line
	8450 3700 8250 3700
Text Label 8450 4000 0    10   ~ 0
GND
Wire Wire Line
	7450 4000 7450 3800
Wire Wire Line
	7450 3800 7250 3800
Text Label 7450 4000 0    10   ~ 0
GND
Wire Wire Line
	7450 4700 7450 4500
Wire Wire Line
	7450 4500 7250 4500
Text Label 7450 4700 0    10   ~ 0
GND
Wire Wire Line
	8450 4900 8150 4900
Text Label 8150 4900 0    70   ~ 0
D4
Wire Wire Line
	8450 4200 8150 4200
Text Label 8150 4200 0    70   ~ 0
D5
Wire Wire Line
	7450 4900 7050 4900
Text Label 7050 4900 0    70   ~ 0
D6
Wire Wire Line
	7450 4100 6950 4100
Text Label 7450 4100 0    10   ~ 0
+5V
Wire Wire Line
	8450 4100 8050 4100
Wire Wire Line
	8050 4100 8050 4000
Text Label 8450 4100 0    10   ~ 0
+5V
Wire Wire Line
	8450 4800 8050 4800
Wire Wire Line
	8050 4800 8050 4700
Text Label 8450 4800 0    10   ~ 0
+5V
Wire Wire Line
	7450 4800 6950 4800
Text Label 7450 4800 0    10   ~ 0
+5V
Wire Wire Line
	7450 4200 7050 4200
Text Label 7050 4200 0    70   ~ 0
D11
Text Label 7050 4200 0    70   ~ 0
D11
$Comp
L RAMPS_1-41_eagle9-eagle-import:3PIN-HEADER SER?
U 1 0 5BE923BF
P 8750 4800
AR Path="/5BE923BF" Ref="SER?"  Part="1" 
AR Path="/5BD77412/5BE923BF" Ref="SER4"  Part="1" 
F 0 "SER4" H 8700 5030 59  0000 L BNN
F 1 "3PIN-HEADER" H 8700 4500 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:3PIN-HEADER" H 8750 4800 50  0001 C CNN
F 3 "" H 8750 4800 50  0001 C CNN
	1    8750 4800
	-1   0    0    1   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:3PIN-HEADER SER?
U 1 0 5BE923C6
P 8750 4100
AR Path="/5BE923C6" Ref="SER?"  Part="1" 
AR Path="/5BD77412/5BE923C6" Ref="SER3"  Part="1" 
F 0 "SER3" H 8700 4330 59  0000 L BNN
F 1 "3PIN-HEADER" H 8700 3800 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:3PIN-HEADER" H 8750 4100 50  0001 C CNN
F 3 "" H 8750 4100 50  0001 C CNN
	1    8750 4100
	-1   0    0    1   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:3PIN-HEADER SER?
U 1 0 5BE923CD
P 7750 4800
AR Path="/5BE923CD" Ref="SER?"  Part="1" 
AR Path="/5BD77412/5BE923CD" Ref="SER2"  Part="1" 
F 0 "SER2" H 7700 5030 59  0000 L BNN
F 1 "3PIN-HEADER" H 7700 4500 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:3PIN-HEADER" H 7750 4800 50  0001 C CNN
F 3 "" H 7750 4800 50  0001 C CNN
	1    7750 4800
	-1   0    0    1   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BE923D4
P 8250 3800
AR Path="/5BE923D4" Ref="#GND?"  Part="1" 
AR Path="/5BD77412/5BE923D4" Ref="#GND0123"  Part="1" 
F 0 "#GND0123" H 8250 3800 50  0001 C CNN
F 1 "GND" H 8150 3700 59  0000 L BNN
F 2 "" H 8250 3800 50  0001 C CNN
F 3 "" H 8250 3800 50  0001 C CNN
	1    8250 3800
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BE923DA
P 8250 4600
AR Path="/5BE923DA" Ref="#GND?"  Part="1" 
AR Path="/5BD77412/5BE923DA" Ref="#GND0124"  Part="1" 
F 0 "#GND0124" H 8250 4600 50  0001 C CNN
F 1 "GND" H 8150 4500 59  0000 L BNN
F 2 "" H 8250 4600 50  0001 C CNN
F 3 "" H 8250 4600 50  0001 C CNN
	1    8250 4600
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:3PIN-HEADER SER?
U 1 0 5BE923E0
P 7750 4100
AR Path="/5BE923E0" Ref="SER?"  Part="1" 
AR Path="/5BD77412/5BE923E0" Ref="SER1"  Part="1" 
F 0 "SER1" H 7700 4330 59  0000 L BNN
F 1 "3PIN-HEADER" H 7700 3800 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:3PIN-HEADER" H 7750 4100 50  0001 C CNN
F 3 "" H 7750 4100 50  0001 C CNN
	1    7750 4100
	-1   0    0    1   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BE923E7
P 7250 4600
AR Path="/5BE923E7" Ref="#GND?"  Part="1" 
AR Path="/5BD77412/5BE923E7" Ref="#GND0125"  Part="1" 
F 0 "#GND0125" H 7250 4600 50  0001 C CNN
F 1 "GND" H 7150 4500 59  0000 L BNN
F 2 "" H 7250 4600 50  0001 C CNN
F 3 "" H 7250 4600 50  0001 C CNN
	1    7250 4600
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BE923ED
P 7250 3900
AR Path="/5BE923ED" Ref="#GND?"  Part="1" 
AR Path="/5BD77412/5BE923ED" Ref="#GND0126"  Part="1" 
F 0 "#GND0126" H 7250 3900 50  0001 C CNN
F 1 "GND" H 7150 3800 59  0000 L BNN
F 2 "" H 7250 3900 50  0001 C CNN
F 3 "" H 7250 3900 50  0001 C CNN
	1    7250 3900
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:+5V #P+?
U 1 0 5BE923F3
P 6950 4000
AR Path="/5BE923F3" Ref="#P+?"  Part="1" 
AR Path="/5BD77412/5BE923F3" Ref="#P+0132"  Part="1" 
F 0 "#P+0132" H 6950 4000 50  0001 C CNN
F 1 "+5V" V 6850 3800 59  0000 L BNN
F 2 "" H 6950 4000 50  0001 C CNN
F 3 "" H 6950 4000 50  0001 C CNN
	1    6950 4000
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:+5V #P+?
U 1 0 5BE923F9
P 8050 3900
AR Path="/5BE923F9" Ref="#P+?"  Part="1" 
AR Path="/5BD77412/5BE923F9" Ref="#P+0133"  Part="1" 
F 0 "#P+0133" H 8050 3900 50  0001 C CNN
F 1 "+5V" V 7950 3700 59  0000 L BNN
F 2 "" H 8050 3900 50  0001 C CNN
F 3 "" H 8050 3900 50  0001 C CNN
	1    8050 3900
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:+5V #P+?
U 1 0 5BE923FF
P 8050 4600
AR Path="/5BE923FF" Ref="#P+?"  Part="1" 
AR Path="/5BD77412/5BE923FF" Ref="#P+0134"  Part="1" 
F 0 "#P+0134" H 8050 4600 50  0001 C CNN
F 1 "+5V" V 7950 4400 59  0000 L BNN
F 2 "" H 8050 4600 50  0001 C CNN
F 3 "" H 8050 4600 50  0001 C CNN
	1    8050 4600
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:+5V #P+?
U 1 0 5BE92405
P 6950 4700
AR Path="/5BE92405" Ref="#P+?"  Part="1" 
AR Path="/5BD77412/5BE92405" Ref="#P+0135"  Part="1" 
F 0 "#P+0135" H 6950 4700 50  0001 C CNN
F 1 "+5V" V 6850 4500 59  0000 L BNN
F 2 "" H 6950 4700 50  0001 C CNN
F 3 "" H 6950 4700 50  0001 C CNN
	1    6950 4700
	1    0    0    -1  
$EndComp
Text Notes 7250 3300 0    170  ~ 0
Servos
Text Notes 7250 3300 0    170  ~ 0
Servos
$EndSCHEMATC
