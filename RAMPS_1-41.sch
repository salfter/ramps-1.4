EESchema Schematic File Version 4
LIBS:RAMPS_1-41-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 6
Title "Reprap Arduino Mega Pololu Shield"
Date "2018-08-21"
Rev "1.4"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	3650 4100 3450 4100
Wire Wire Line
	3450 4200 3450 4100
Wire Wire Line
	3650 4200 3450 4200
Wire Wire Line
	2750 4100 3450 4100
Wire Wire Line
	2750 4100 2750 4400
Wire Wire Line
	2750 4400 2750 4500
Wire Wire Line
	2750 4500 2750 4600
Wire Wire Line
	3650 4400 2750 4400
Wire Wire Line
	3650 4500 2750 4500
Connection ~ 3450 4100
Connection ~ 2750 4400
Connection ~ 2750 4500
Text Label 3650 4100 0    10   ~ 0
GND
Wire Wire Line
	2850 3700 3650 3700
Wire Wire Line
	2850 3700 2850 3500
Text Label 2850 3700 0    10   ~ 0
VCC
Wire Wire Line
	3650 4600 2950 4600
Wire Wire Line
	2950 4600 2950 4700
Wire Wire Line
	3650 4700 2950 4700
Wire Wire Line
	2950 4700 2950 4800
Connection ~ 2950 4700
Text Label 3650 4600 0    10   ~ 0
VCC
Wire Wire Line
	4650 4700 5350 4700
Text Label 4850 4700 0    70   ~ 0
E0-STEP
Wire Wire Line
	4650 4500 5350 4500
Text Label 4850 4500 0    70   ~ 0
E0-EN
Wire Wire Line
	4650 4900 5350 4900
Text Label 4850 4900 0    70   ~ 0
E0-DIR
Wire Wire Line
	4650 5900 5350 5900
Text Label 4850 5900 0    70   ~ 0
X-EN
Wire Wire Line
	3650 1800 3150 1800
Text Label 3150 1800 0    70   ~ 0
X-STEP
Wire Wire Line
	3650 1900 3150 1900
Text Label 3150 1900 0    70   ~ 0
X-DIR
Wire Wire Line
	3650 2000 3150 2000
Text Label 3150 2000 0    70   ~ 0
Y-EN
Wire Wire Line
	3650 2400 3150 2400
Text Label 3150 2400 0    70   ~ 0
Y-STEP
Wire Wire Line
	3650 2500 3150 2500
Text Label 3150 2500 0    70   ~ 0
Y-DIR
Wire Wire Line
	4650 2200 5350 2200
Text Label 4850 2200 0    70   ~ 0
X-MIN
Wire Wire Line
	4650 3400 5350 3400
Text Label 4850 3400 0    70   ~ 0
Y-MIN
Wire Wire Line
	4650 3800 5350 3800
Text Label 4850 3800 0    70   ~ 0
Z-MIN
Wire Wire Line
	4650 2100 5350 2100
Text Label 4850 2100 0    70   ~ 0
X-MAX
Wire Wire Line
	4650 3500 5350 3500
Text Label 4850 3500 0    70   ~ 0
Y-MAX
Wire Wire Line
	4650 3900 5350 3900
Text Label 4850 3900 0    70   ~ 0
Z-MAX
Wire Wire Line
	4650 5700 5350 5700
Text Label 4850 5700 0    70   ~ 0
E1-STEP
Wire Wire Line
	4650 5100 5350 5100
Text Label 4850 5100 0    70   ~ 0
E1-EN
Wire Wire Line
	3650 3100 3150 3100
Text Label 3150 3100 0    70   ~ 0
THERM0
Wire Wire Line
	4650 3200 5350 3200
Text Label 4850 3200 0    70   ~ 0
LED
Wire Wire Line
	3650 3500 3150 3500
Text Label 3150 3500 0    70   ~ 0
RESET
Wire Wire Line
	3650 3200 3150 3200
Text Label 3150 3200 0    70   ~ 0
THERM1
Wire Wire Line
	3650 3600 3150 3600
Text Label 3150 3600 0    70   ~ 0
AM-VIN
Wire Wire Line
	4650 2900 5350 2900
Text Label 4850 2900 0    70   ~ 0
D10
Wire Wire Line
	3650 5100 2950 5100
Text Label 2950 5100 0    70   ~ 0
D43
Wire Wire Line
	3650 5300 2950 5300
Text Label 2950 5300 0    70   ~ 0
D45
Wire Wire Line
	3650 5500 2950 5500
Text Label 2950 5500 0    70   ~ 0
D47
Wire Wire Line
	3650 5700 2950 5700
Text Label 2950 5700 0    70   ~ 0
D49
Wire Wire Line
	3650 6100 2950 6100
Text Label 2950 6100 0    70   ~ 0
D53
Wire Wire Line
	3650 4900 3250 4900
Text Label 3250 4900 0    70   ~ 0
D41
Wire Wire Line
	4650 6000 5350 6000
Text Label 4850 6000 0    70   ~ 0
D39
Wire Wire Line
	4650 5800 5350 5800
Text Label 4850 5800 0    70   ~ 0
D37
Wire Wire Line
	4650 5600 5350 5600
Text Label 4850 5600 0    70   ~ 0
D35
Wire Wire Line
	4650 5400 5350 5400
Text Label 4850 5400 0    70   ~ 0
D33
Wire Wire Line
	4650 5200 5350 5200
Text Label 4850 5200 0    70   ~ 0
D31
Wire Wire Line
	4650 5000 5350 5000
Text Label 4850 5000 0    70   ~ 0
D29
Wire Wire Line
	4650 4800 5350 4800
Text Label 4850 4800 0    70   ~ 0
D27
Wire Wire Line
	4650 4600 5350 4600
Text Label 4850 4600 0    70   ~ 0
D25
Wire Wire Line
	4650 4400 5350 4400
Text Label 4850 4400 0    70   ~ 0
D23
Wire Wire Line
	4650 2300 5350 2300
Text Label 4850 2300 0    70   ~ 0
D4
Wire Wire Line
	4650 2400 5350 2400
Text Label 4850 2400 0    70   ~ 0
D5
Wire Wire Line
	4650 2700 5350 2700
Text Label 4850 2700 0    70   ~ 0
D8
Wire Wire Line
	4650 2800 5350 2800
Text Label 4850 2800 0    70   ~ 0
D9
Wire Wire Line
	4650 4000 5350 4000
Text Label 4850 4000 0    70   ~ 0
SDA
Wire Wire Line
	4650 4100 5350 4100
Text Label 4850 4100 0    70   ~ 0
SCL
Wire Wire Line
	4650 3600 5350 3600
Text Label 4850 3600 0    70   ~ 0
D16
Wire Wire Line
	4650 3700 5350 3700
Text Label 4850 3700 0    70   ~ 0
D17
Wire Wire Line
	4650 5500 5350 5500
Text Label 4850 5500 0    70   ~ 0
E1-DIR
Wire Wire Line
	3650 2600 3150 2600
Text Label 3150 2600 0    70   ~ 0
Z-EN
Wire Wire Line
	3650 5400 2950 5400
Text Label 2950 5400 0    70   ~ 0
Z-STEP
Wire Wire Line
	3650 5600 2950 5600
Text Label 2950 5600 0    70   ~ 0
Z-DIR
Wire Wire Line
	3650 5800 2950 5800
Text Label 2950 5800 0    70   ~ 0
MISO
Wire Wire Line
	3650 6000 2950 6000
Text Label 2950 6000 0    70   ~ 0
SCK
Wire Wire Line
	3650 5900 2950 5900
Text Label 2950 5900 0    70   ~ 0
MOSI
Wire Wire Line
	3650 3300 3150 3300
Text Label 3150 3300 0    70   ~ 0
THERM2
Wire Wire Line
	4650 5300 5350 5300
Text Label 4850 5300 0    70   ~ 0
D32
Wire Wire Line
	4650 1800 5350 1800
Text Label 4850 1800 0    70   ~ 0
D1
Wire Wire Line
	4650 1900 5350 1900
Text Label 4850 1900 0    70   ~ 0
D2
Wire Wire Line
	4650 3100 5350 3100
Text Label 4850 3100 0    70   ~ 0
D12
Wire Wire Line
	4650 2500 5350 2500
Text Label 4875 2500 0    70   ~ 0
D6
Wire Wire Line
	3650 2100 3150 2100
Text Label 3150 2100 0    70   ~ 0
A3
Wire Wire Line
	3650 2200 3150 2200
Text Label 3150 2200 0    70   ~ 0
A4
Wire Wire Line
	3650 2300 3150 2300
Text Label 3150 2300 0    70   ~ 0
A5
Wire Wire Line
	3650 2800 3150 2800
Text Label 3150 2800 0    70   ~ 0
A10
Wire Wire Line
	3650 2900 3150 2900
Text Label 3150 2900 0    70   ~ 0
A11
Wire Wire Line
	3650 2700 3150 2700
Text Label 3150 2700 0    70   ~ 0
A9
Wire Wire Line
	4650 6100 5350 6100
Text Label 4850 6100 0    70   ~ 0
D40
Wire Wire Line
	3650 5000 3250 5000
Text Label 3250 5000 0    70   ~ 0
D42
Wire Wire Line
	3650 5200 2950 5200
Text Label 2950 5200 0    70   ~ 0
D44
Wire Wire Line
	3650 3000 3150 3000
Text Label 3150 3000 0    70   ~ 0
A12
Wire Wire Line
	4650 3000 5350 3000
Text Label 4850 3000 0    70   ~ 0
D11
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND016
U 1 0 12C6E2A1
P 2750 4700
F 0 "#GND016" H 2750 4700 50  0001 C CNN
F 1 "GND" H 2650 4600 59  0000 L BNN
F 2 "" H 2750 4700 50  0001 C CNN
F 3 "" H 2750 4700 50  0001 C CNN
	1    2750 4700
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+010
U 1 0 6665561A
P 2850 3400
F 0 "#P+010" H 2850 3400 50  0001 C CNN
F 1 "VCC" V 2750 3300 59  0000 L BNN
F 2 "" H 2850 3400 50  0001 C CNN
F 3 "" H 2850 3400 50  0001 C CNN
	1    2850 3400
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+011
U 1 0 4D46F757
P 2950 4900
F 0 "#P+011" H 2950 4900 50  0001 C CNN
F 1 "VCC" V 2850 4800 59  0000 L BNN
F 2 "" H 2950 4900 50  0001 C CNN
F 3 "" H 2950 4900 50  0001 C CNN
	1    2950 4900
	-1   0    0    1   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:ARDUINO_MEGA_SHIELD U1
U 1 0 94FABF20
P 4150 2500
F 0 "U1" H 3770 3330 59  0000 L BNN
F 1 "ARDUINO_MEGA_SHIELD" H 3800 -1300 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:ARDUINO_MEGA_SHIELD" H 4150 2500 50  0001 C CNN
F 3 "" H 4150 2500 50  0001 C CNN
	1    4150 2500
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:OSHW_LOGO_6MILX0100-NT LOGO1
U 1 0 994F8438
P 8100 4700
F 0 "LOGO1" H 8100 4700 50  0001 C CNN
F 1 "OSHW_LOGO_6MILX0100-NT" H 8100 4700 50  0001 C CNN
F 2 "RAMPS_1-41_eagle9:OSHW_6X100_NOTEXT" H 8100 4700 50  0001 C CNN
F 3 "" H 8100 4700 50  0001 C CNN
	1    8100 4700
	1    0    0    -1  
$EndComp
Text Notes 2650 1500 0    170  ~ 0
MEGA Conn.
Text GLabel 3650 4000 0    10   BiDi ~ 0
GND
$Sheet
S 6200 2450 1500 850 
U 5BA9E9B7
F0 "Stepper Drivers" 50
F1 "stepper_drivers.sch" 50
$EndSheet
$Sheet
S 6200 3500 1500 850 
U 5BD1C179
F0 "Heaters & Fans" 50
F1 "heaters_fans.sch" 50
$EndSheet
$Sheet
S 6200 4600 1500 850 
U 5BD77412
F0 "Endstops, Thermistors, & Servos" 50
F1 "endstops.sch" 50
$EndSheet
$Sheet
S 7900 2450 1500 850 
U 5BDCB418
F0 "Power, LED, & Reset" 50
F1 "power.sch" 50
$EndSheet
$Sheet
S 7900 3500 1500 850 
U 5BE9C6A1
F0 "Expansion" 50
F1 "expansion.sch" 50
$EndSheet
$EndSCHEMATC
