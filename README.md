RepRap Arduino Mega Pololu Shield
=================================

This is primarily a conversion of the original EAGLE files available from
https://reprap.org/wiki/RAMPS_1.4#Source to KiCad.  The conversion was done
with the converter built into KiCad 5.0; so far, this converter hasn't
failed me (built a RAMPS-FD 2.2 after converting it from EAGLE to KiCad, and
it's a more complex board than this).

