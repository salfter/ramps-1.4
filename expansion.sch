EESchema Schematic File Version 4
LIBS:RAMPS_1-41-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 6 6
Title "Reprap Arduino Mega Pololu Shield"
Date "2018-08-21"
Rev "1.4"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	1650 3900 1650 3600
Wire Wire Line
	1650 3600 1450 3600
Wire Wire Line
	1650 3900 2050 3900
Text Label 1650 3900 0    10   ~ 0
GND
Wire Wire Line
	2050 4900 1550 4900
Wire Wire Line
	1550 4900 1550 4600
Wire Wire Line
	1550 4600 1350 4600
Text Label 2050 4900 0    10   ~ 0
GND
Wire Wire Line
	2050 3800 1750 3800
Wire Wire Line
	1750 3800 1750 3400
Text Label 2050 3800 0    10   ~ 0
VCC
Wire Wire Line
	2050 4800 1750 4800
Wire Wire Line
	1750 4800 1750 4500
Text Label 2050 4800 0    10   ~ 0
VCC
Wire Wire Line
	2050 4100 1350 4100
Text Label 1350 4100 0    70   ~ 0
D1
Wire Wire Line
	2050 4000 1350 4000
Text Label 1350 4000 0    70   ~ 0
D2
Wire Wire Line
	2050 5000 1350 5000
Text Label 1350 5000 0    70   ~ 0
A3
Wire Wire Line
	2050 5100 1350 5100
Text Label 1350 5100 0    70   ~ 0
A4
$Comp
L RAMPS_1-41_eagle9-eagle-import:PINHD-1X4 SERIAL?
U 1 0 5BEA4FDF
P 2150 4900
AR Path="/5BEA4FDF" Ref="SERIAL?"  Part="1" 
AR Path="/5BE9C6A1/5BEA4FDF" Ref="SERIAL1"  Part="1" 
F 0 "SERIAL1" H 1900 5125 59  0000 L BNN
F 1 "PINHD-1X4" H 1900 4500 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:1X04" H 2150 4900 50  0001 C CNN
F 3 "" H 2150 4900 50  0001 C CNN
	1    2150 4900
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:PINHD-1X4 A-OUT?
U 1 0 5BEA4FE6
P 2150 3900
AR Path="/5BEA4FE6" Ref="A-OUT?"  Part="1" 
AR Path="/5BE9C6A1/5BEA4FE6" Ref="A-OUT1"  Part="1" 
F 0 "A-OUT1" H 1900 4125 59  0000 L BNN
F 1 "PINHD-1X4" H 1900 3500 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:1X04" H 2150 3900 50  0001 C CNN
F 3 "" H 2150 3900 50  0001 C CNN
	1    2150 3900
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BEA4FED
P 1450 3700
AR Path="/5BEA4FED" Ref="#GND?"  Part="1" 
AR Path="/5BE9C6A1/5BEA4FED" Ref="#GND0133"  Part="1" 
F 0 "#GND0133" H 1450 3700 50  0001 C CNN
F 1 "GND" H 1350 3600 59  0000 L BNN
F 2 "" H 1450 3700 50  0001 C CNN
F 3 "" H 1450 3700 50  0001 C CNN
	1    1450 3700
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BEA4FF3
P 1750 3300
AR Path="/5BEA4FF3" Ref="#P+?"  Part="1" 
AR Path="/5BE9C6A1/5BEA4FF3" Ref="#P+0142"  Part="1" 
F 0 "#P+0142" H 1750 3300 50  0001 C CNN
F 1 "VCC" V 1650 3200 59  0000 L BNN
F 2 "" H 1750 3300 50  0001 C CNN
F 3 "" H 1750 3300 50  0001 C CNN
	1    1750 3300
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BEA4FF9
P 1750 4400
AR Path="/5BEA4FF9" Ref="#P+?"  Part="1" 
AR Path="/5BE9C6A1/5BEA4FF9" Ref="#P+0143"  Part="1" 
F 0 "#P+0143" H 1750 4400 50  0001 C CNN
F 1 "VCC" V 1650 4300 59  0000 L BNN
F 2 "" H 1750 4400 50  0001 C CNN
F 3 "" H 1750 4400 50  0001 C CNN
	1    1750 4400
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BEA4FFF
P 1350 4700
AR Path="/5BEA4FFF" Ref="#GND?"  Part="1" 
AR Path="/5BE9C6A1/5BEA4FFF" Ref="#GND0134"  Part="1" 
F 0 "#GND0134" H 1350 4700 50  0001 C CNN
F 1 "GND" H 1250 4600 59  0000 L BNN
F 2 "" H 1350 4700 50  0001 C CNN
F 3 "" H 1350 4700 50  0001 C CNN
	1    1350 4700
	1    0    0    -1  
$EndComp
Text Notes 1350 3100 0    170  ~ 0
AUX-1
Wire Wire Line
	3600 4000 4100 4000
Text Label 3600 4000 0    10   ~ 0
GND
Wire Wire Line
	3000 4000 2900 4000
Wire Wire Line
	2900 4000 2900 3800
Text Label 3000 4000 0    10   ~ 0
VCC
Wire Wire Line
	3000 4100 2800 4100
Text Label 2800 4100 0    70   ~ 0
A5
Wire Wire Line
	3000 4200 2800 4200
Text Label 2800 4200 0    70   ~ 0
A10
Wire Wire Line
	3600 4400 3900 4400
Text Label 3700 4400 0    70   ~ 0
A11
Wire Wire Line
	3600 4100 3900 4100
Text Label 3700 4100 0    70   ~ 0
A9
Wire Wire Line
	3600 4200 3900 4200
Text Label 3700 4200 0    70   ~ 0
D40
Wire Wire Line
	3600 4300 3900 4300
Text Label 3700 4300 0    70   ~ 0
D42
Wire Wire Line
	3000 4300 2800 4300
Text Label 2800 4300 0    70   ~ 0
D44
Wire Wire Line
	3000 4400 2800 4400
Text Label 2800 4400 0    70   ~ 0
A12
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BEAC6B3
P 2900 3700
AR Path="/5BEAC6B3" Ref="#P+?"  Part="1" 
AR Path="/5BE9C6A1/5BEAC6B3" Ref="#P+0144"  Part="1" 
F 0 "#P+0144" H 2900 3700 50  0001 C CNN
F 1 "VCC" V 2800 3600 59  0000 L BNN
F 2 "" H 2900 3700 50  0001 C CNN
F 3 "" H 2900 3700 50  0001 C CNN
	1    2900 3700
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BEAC6B9
P 4100 4100
AR Path="/5BEAC6B9" Ref="#GND?"  Part="1" 
AR Path="/5BE9C6A1/5BEAC6B9" Ref="#GND0135"  Part="1" 
F 0 "#GND0135" H 4100 4100 50  0001 C CNN
F 1 "GND" H 4000 4000 59  0000 L BNN
F 2 "" H 4100 4100 50  0001 C CNN
F 3 "" H 4100 4100 50  0001 C CNN
	1    4100 4100
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:M05X2 U$?
U 1 0 5BEAC6BF
P 3300 4200
AR Path="/5BEAC6BF" Ref="U$?"  Part="1" 
AR Path="/5BE9C6A1/5BEAC6BF" Ref="U$10"  Part="1" 
F 0 "U$10" H 3140 4530 59  0000 L BNN
F 1 "M05X2" H 3120 3800 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:2X5" H 3300 4200 50  0001 C CNN
F 3 "" H 3300 4200 50  0001 C CNN
	1    3300 4200
	1    0    0    -1  
$EndComp
Text Notes 4110 3150 0    127  ~ 0
" "
Text Notes 3000 3100 0    170  ~ 0
AUX-2
Wire Wire Line
	4750 4500 4750 4300
Wire Wire Line
	4750 4300 4950 4300
Text Label 4750 4500 0    10   ~ 0
GND
Wire Wire Line
	4750 3700 4750 4000
Wire Wire Line
	4750 4000 4950 4000
Text Label 4750 3700 0    10   ~ 0
VCC
Wire Wire Line
	5550 4000 5950 4000
Text Label 5650 4000 0    70   ~ 0
D49
Wire Wire Line
	5550 4200 5950 4200
Text Label 5650 4200 0    70   ~ 0
D53
Wire Wire Line
	4950 4100 4450 4100
Text Label 4550 4100 0    70   ~ 0
MISO
Wire Wire Line
	4950 4200 4450 4200
Text Label 4550 4200 0    70   ~ 0
SCK
Wire Wire Line
	5550 4100 5950 4100
Text Label 5650 4100 0    70   ~ 0
MOSI
$Comp
L RAMPS_1-41_eagle9-eagle-import:M04X2 U$?
U 1 0 5BEB3136
P 5250 4200
AR Path="/5BEB3136" Ref="U$?"  Part="1" 
AR Path="/5BE9C6A1/5BEB3136" Ref="U$3"  Part="1" 
F 0 "U$3" H 5090 4530 59  0000 L BNN
F 1 "M04X2" H 5070 3900 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:2X4" H 5250 4200 50  0001 C CNN
F 3 "" H 5250 4200 50  0001 C CNN
	1    5250 4200
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BEB313D
P 4750 4600
AR Path="/5BEB313D" Ref="#GND?"  Part="1" 
AR Path="/5BE9C6A1/5BEB313D" Ref="#GND0136"  Part="1" 
F 0 "#GND0136" H 4750 4600 50  0001 C CNN
F 1 "GND" H 4650 4500 59  0000 L BNN
F 2 "" H 4750 4600 50  0001 C CNN
F 3 "" H 4750 4600 50  0001 C CNN
	1    4750 4600
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BEB3143
P 4750 3600
AR Path="/5BEB3143" Ref="#P+?"  Part="1" 
AR Path="/5BE9C6A1/5BEB3143" Ref="#P+0145"  Part="1" 
F 0 "#P+0145" H 4750 3600 50  0001 C CNN
F 1 "VCC" V 4650 3500 59  0000 L BNN
F 2 "" H 4750 3600 50  0001 C CNN
F 3 "" H 4750 3600 50  0001 C CNN
	1    4750 3600
	1    0    0    -1  
$EndComp
Text Notes 4750 3100 0    170  ~ 0
AUX-3
Wire Wire Line
	6800 4900 6300 4900
Wire Wire Line
	6300 4900 6300 5000
Text Label 6800 4900 0    10   ~ 0
GND
Wire Wire Line
	6600 5200 6700 5200
Wire Wire Line
	6700 5200 6700 5000
Wire Wire Line
	6700 5000 6800 5000
Text Label 6600 5200 0    10   ~ 0
VCC
Wire Wire Line
	6800 4500 6400 4500
Text Label 6400 4500 0    70   ~ 0
D43
Wire Wire Line
	6800 4600 6400 4600
Text Label 6400 4600 0    70   ~ 0
D45
Wire Wire Line
	6800 4700 6400 4700
Text Label 6400 4700 0    70   ~ 0
D47
Wire Wire Line
	6800 4400 6400 4400
Text Label 6400 4400 0    70   ~ 0
D41
Wire Wire Line
	6800 4300 6400 4300
Text Label 6400 4300 0    70   ~ 0
D39
Wire Wire Line
	6800 4200 6400 4200
Text Label 6400 4200 0    70   ~ 0
D37
Wire Wire Line
	6800 4100 6400 4100
Text Label 6400 4100 0    70   ~ 0
D35
Wire Wire Line
	6800 4000 6400 4000
Text Label 6400 4000 0    70   ~ 0
D33
Wire Wire Line
	6800 3900 6400 3900
Text Label 6400 3900 0    70   ~ 0
D31
Wire Wire Line
	6400 3800 6800 3800
Text Label 6400 3800 0    70   ~ 0
D29
Wire Wire Line
	6800 3700 6400 3700
Text Label 6400 3700 0    70   ~ 0
D27
Wire Wire Line
	6400 3600 6800 3600
Text Label 6400 3600 0    70   ~ 0
D25
Wire Wire Line
	6800 3500 6400 3500
Text Label 6400 3500 0    70   ~ 0
D23
Wire Wire Line
	6800 3300 6400 3300
Text Label 6400 3300 0    70   ~ 0
D16
Wire Wire Line
	6800 3400 6400 3400
Text Label 6400 3400 0    70   ~ 0
D17
Wire Wire Line
	6800 4800 6400 4800
Text Label 6400 4800 0    70   ~ 0
D32
$Comp
L RAMPS_1-41_eagle9-eagle-import:M18_ U$?
U 1 0 5BEB7C89
P 7100 4100
AR Path="/5BEB7C89" Ref="U$?"  Part="1" 
AR Path="/5BE9C6A1/5BEB7C89" Ref="U$9"  Part="1" 
F 0 "U$9" H 6900 3250 59  0000 R TNN
F 1 "M18_" H 7000 3000 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:1X18" H 7100 4100 50  0001 C CNN
F 3 "" H 7100 4100 50  0001 C CNN
	1    7100 4100
	-1   0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BEB7C90
P 6600 5100
AR Path="/5BEB7C90" Ref="#P+?"  Part="1" 
AR Path="/5BE9C6A1/5BEB7C90" Ref="#P+0146"  Part="1" 
F 0 "#P+0146" H 6600 5100 50  0001 C CNN
F 1 "VCC" V 6500 5000 59  0000 L BNN
F 2 "" H 6600 5100 50  0001 C CNN
F 3 "" H 6600 5100 50  0001 C CNN
	1    6600 5100
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BEB7C96
P 6300 5100
AR Path="/5BEB7C96" Ref="#GND?"  Part="1" 
AR Path="/5BE9C6A1/5BEB7C96" Ref="#GND0137"  Part="1" 
F 0 "#GND0137" H 6300 5100 50  0001 C CNN
F 1 "GND" H 6200 5000 59  0000 L BNN
F 2 "" H 6300 5100 50  0001 C CNN
F 3 "" H 6300 5100 50  0001 C CNN
	1    6300 5100
	1    0    0    -1  
$EndComp
Text Notes 6300 3100 0    170  ~ 0
AUX-4
Wire Wire Line
	9150 4100 7950 4100
Text Label 9150 4100 0    10   ~ 0
GND
Wire Wire Line
	8150 3600 8450 3600
Wire Wire Line
	8450 3600 8750 3600
Wire Wire Line
	8750 3600 8950 3600
Wire Wire Line
	8950 3600 8950 4000
Wire Wire Line
	8950 4000 9150 4000
Connection ~ 8450 3600
Connection ~ 8750 3600
Text Label 8150 3600 0    10   ~ 0
VCC
Wire Wire Line
	9150 4200 8750 4200
Wire Wire Line
	8750 4000 8750 4200
Wire Wire Line
	8750 4200 8150 4200
Connection ~ 8750 4200
Text Label 8150 4200 0    70   ~ 0
SDA
Wire Wire Line
	9150 4300 8450 4300
Wire Wire Line
	8450 4000 8450 4300
Wire Wire Line
	8450 4300 8150 4300
Connection ~ 8450 4300
Text Label 8150 4300 0    70   ~ 0
SCL
$Comp
L RAMPS_1-41_eagle9-eagle-import:PINHD-1X4 I2C?
U 1 0 5BEBC32B
P 9250 4100
AR Path="/5BEBC32B" Ref="I2C?"  Part="1" 
AR Path="/5BE9C6A1/5BEBC32B" Ref="I2C1"  Part="1" 
F 0 "I2C1" H 9000 4325 59  0000 L BNN
F 1 "PINHD-1X4" H 9000 3700 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:1X04" H 9250 4100 50  0001 C CNN
F 3 "" H 9250 4100 50  0001 C CNN
	1    9250 4100
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BEBC332
P 7950 4200
AR Path="/5BEBC332" Ref="#GND?"  Part="1" 
AR Path="/5BE9C6A1/5BEBC332" Ref="#GND0138"  Part="1" 
F 0 "#GND0138" H 7950 4200 50  0001 C CNN
F 1 "GND" H 7850 4100 59  0000 L BNN
F 2 "" H 7950 4200 50  0001 C CNN
F 3 "" H 7950 4200 50  0001 C CNN
	1    7950 4200
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:VCC #P+?
U 1 0 5BEBC338
P 8150 3500
AR Path="/5BEBC338" Ref="#P+?"  Part="1" 
AR Path="/5BE9C6A1/5BEBC338" Ref="#P+0147"  Part="1" 
F 0 "#P+0147" H 8150 3500 50  0001 C CNN
F 1 "VCC" V 8050 3400 59  0000 L BNN
F 2 "" H 8150 3500 50  0001 C CNN
F 3 "" H 8150 3500 50  0001 C CNN
	1    8150 3500
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R?
U 1 0 5BEBC33E
P 8750 3800
AR Path="/5BEBC33E" Ref="R?"  Part="1" 
AR Path="/5BE9C6A1/5BEBC33E" Ref="R21"  Part="1" 
F 0 "R21" H 8600 3859 59  0000 L BNN
F 1 "4.7K" H 8600 3670 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 8750 3800 50  0001 C CNN
F 3 "" H 8750 3800 50  0001 C CNN
	1    8750 3800
	0    -1   -1   0   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R?
U 1 0 5BEBC345
P 8450 3800
AR Path="/5BEBC345" Ref="R?"  Part="1" 
AR Path="/5BE9C6A1/5BEBC345" Ref="R22"  Part="1" 
F 0 "R22" H 8300 3859 59  0000 L BNN
F 1 "4.7K" H 8300 3670 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 8450 3800 50  0001 C CNN
F 3 "" H 8450 3800 50  0001 C CNN
	1    8450 3800
	0    -1   -1   0   
$EndComp
Text Notes 8250 3100 0    170  ~ 0
I2C
$EndSCHEMATC
