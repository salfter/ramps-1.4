EESchema Schematic File Version 4
LIBS:RAMPS_1-41-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 3 6
Title "Reprap Arduino Mega Pololu Shield"
Date "2018-08-21"
Rev "1.4"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5200 2850 5200 2950
Connection ~ 5200 2950
Text Label 5200 2850 0    10   ~ 0
GND
Wire Wire Line
	5200 3750 5200 3650
Connection ~ 5200 3750
Text Label 5200 3750 0    10   ~ 0
GND
Wire Wire Line
	5200 4450 5200 4550
Wire Wire Line
	5200 4550 5200 4650
Connection ~ 5200 4550
Text Label 5200 4450 0    10   ~ 0
GND
Wire Wire Line
	6200 2750 6200 2950
Wire Wire Line
	6200 2950 6200 3350
Wire Wire Line
	6500 3350 6200 3350
Wire Wire Line
	6500 3750 6200 3750
Wire Wire Line
	6200 3750 6200 3650
Wire Wire Line
	6200 3650 6200 3350
Wire Wire Line
	5900 3650 6200 3650
Wire Wire Line
	5900 2950 6200 2950
Connection ~ 6200 3350
Connection ~ 6200 3650
Connection ~ 6200 2950
Text Label 6200 2750 0    10   ~ 0
+12V
Wire Wire Line
	4400 2750 4200 2750
Wire Wire Line
	4800 2950 4400 2950
Wire Wire Line
	4400 2950 4400 2750
Wire Wire Line
	4500 2750 4400 2750
Connection ~ 4400 2750
Text Label 4200 2750 0    70   ~ 0
D10
Wire Wire Line
	5200 3250 5500 3250
Wire Wire Line
	5500 3250 6300 3250
Wire Wire Line
	6300 3250 6300 3550
Wire Wire Line
	6300 3550 6500 3550
Connection ~ 5500 3250
Wire Wire Line
	5200 4050 5600 4050
Wire Wire Line
	6300 4050 6300 3950
Wire Wire Line
	6300 3950 6500 3950
Connection ~ 5600 4050
Wire Wire Line
	5200 2450 5500 2450
Wire Wire Line
	5500 2450 6300 2450
Wire Wire Line
	6300 2450 6300 3150
Wire Wire Line
	6300 3150 6500 3150
Connection ~ 5500 2450
Wire Wire Line
	4800 4550 4500 4550
Wire Wire Line
	4500 4550 4500 4350
Wire Wire Line
	4500 4350 4300 4350
Connection ~ 4500 4350
Text Label 4300 4350 0    70   ~ 0
D8
Wire Wire Line
	4500 3550 4200 3550
Wire Wire Line
	4800 3750 4500 3750
Wire Wire Line
	4500 3750 4500 3550
Connection ~ 4500 3550
Text Label 4200 3550 0    70   ~ 0
D9
Wire Wire Line
	6500 4150 6200 4150
Wire Wire Line
	6200 4150 6200 4450
Wire Wire Line
	6200 4450 6200 4750
Wire Wire Line
	6200 4750 6500 4750
Wire Wire Line
	6500 4750 6500 4550
Wire Wire Line
	6000 4450 6200 4450
Connection ~ 6200 4450
Text Label 6500 4150 0    10   ~ 0
+12V2
Wire Wire Line
	5000 3550 4900 3550
Wire Wire Line
	5000 2750 4900 2750
Wire Wire Line
	5000 4350 4900 4350
Wire Wire Line
	5600 4450 5600 4350
Wire Wire Line
	5500 3650 5500 3550
Wire Wire Line
	5500 2950 5500 2750
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BD3274C
P 5200 3050
AR Path="/5BD3274C" Ref="#GND?"  Part="1" 
AR Path="/5BD1C179/5BD3274C" Ref="#GND0116"  Part="1" 
F 0 "#GND0116" H 5200 3050 50  0001 C CNN
F 1 "GND" H 5100 2950 59  0000 L BNN
F 2 "" H 5200 3050 50  0001 C CNN
F 3 "" H 5200 3050 50  0001 C CNN
	1    5200 3050
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:IRF512 Q?
U 1 0 5BD32752
P 5100 2650
AR Path="/5BD32752" Ref="Q?"  Part="1" 
AR Path="/5BD1C179/5BD32752" Ref="Q1"  Part="1" 
F 0 "Q1" H 4900 2650 59  0000 L BNN
F 1 "STP55NF06L" H 5200 2850 59  0000 R TNN
F 2 "RAMPS_1-41_eagle9:TO220BV" H 5100 2650 50  0001 C CNN
F 3 "" H 5100 2650 50  0001 C CNN
	1    5100 2650
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:IRF512 Q?
U 1 0 5BD32759
P 5100 3450
AR Path="/5BD32759" Ref="Q?"  Part="1" 
AR Path="/5BD1C179/5BD32759" Ref="Q2"  Part="1" 
F 0 "Q2" H 4900 3450 59  0000 L BNN
F 1 "STP55NF06L" H 5200 3650 59  0000 R TNN
F 2 "RAMPS_1-41_eagle9:TO220BV" H 5100 3450 50  0001 C CNN
F 3 "" H 5100 3450 50  0001 C CNN
	1    5100 3450
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BD32760
P 5200 3850
AR Path="/5BD32760" Ref="#GND?"  Part="1" 
AR Path="/5BD1C179/5BD32760" Ref="#GND0117"  Part="1" 
F 0 "#GND0117" H 5200 3850 50  0001 C CNN
F 1 "GND" H 5100 3750 59  0000 L BNN
F 2 "" H 5200 3850 50  0001 C CNN
F 3 "" H 5200 3850 50  0001 C CNN
	1    5200 3850
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:IRF512 Q?
U 1 0 5BD32766
P 5100 4250
AR Path="/5BD32766" Ref="Q?"  Part="1" 
AR Path="/5BD1C179/5BD32766" Ref="Q3"  Part="1" 
F 0 "Q3" H 4900 4250 59  0000 L BNN
F 1 "STP55NF06L" H 5200 4450 59  0000 R TNN
F 2 "RAMPS_1-41_eagle9:TO220BV" H 5100 4250 50  0001 C CNN
F 3 "" H 5100 4250 50  0001 C CNN
	1    5100 4250
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:+12V #P+?
U 1 0 5BD3276D
P 6200 2650
AR Path="/5BD3276D" Ref="#P+?"  Part="1" 
AR Path="/5BD1C179/5BD3276D" Ref="#P+0126"  Part="1" 
F 0 "#P+0126" H 6200 2650 50  0001 C CNN
F 1 "+12V" V 6100 2450 59  0000 L BNN
F 2 "" H 6200 2650 50  0001 C CNN
F 3 "" H 6200 2650 50  0001 C CNN
	1    6200 2650
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:GND #GND?
U 1 0 5BD32773
P 5200 4750
AR Path="/5BD32773" Ref="#GND?"  Part="1" 
AR Path="/5BD1C179/5BD32773" Ref="#GND0118"  Part="1" 
F 0 "#GND0118" H 5200 4750 50  0001 C CNN
F 1 "GND" H 5100 4650 59  0000 L BNN
F 2 "" H 5200 4750 50  0001 C CNN
F 3 "" H 5200 4750 50  0001 C CNN
	1    5200 4750
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R?
U 1 0 5BD32779
P 5000 3750
AR Path="/5BD32779" Ref="R?"  Part="1" 
AR Path="/5BD1C179/5BD32779" Ref="R2"  Part="1" 
F 0 "R2" H 4850 3809 59  0000 L BNN
F 1 "100k" H 4850 3620 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 5000 3750 50  0001 C CNN
F 3 "" H 5000 3750 50  0001 C CNN
	1    5000 3750
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R?
U 1 0 5BD32780
P 5000 4550
AR Path="/5BD32780" Ref="R?"  Part="1" 
AR Path="/5BD1C179/5BD32780" Ref="R4"  Part="1" 
F 0 "R4" H 4850 4609 59  0000 L BNN
F 1 "100k" H 4850 4420 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 5000 4550 50  0001 C CNN
F 3 "" H 5000 4550 50  0001 C CNN
	1    5000 4550
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R?
U 1 0 5BD32787
P 5000 2950
AR Path="/5BD32787" Ref="R?"  Part="1" 
AR Path="/5BD1C179/5BD32787" Ref="R9"  Part="1" 
F 0 "R9" H 4850 3009 59  0000 L BNN
F 1 "100k" H 4850 2820 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 5000 2950 50  0001 C CNN
F 3 "" H 5000 2950 50  0001 C CNN
	1    5000 2950
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:282837-6 U$?
U 1 0 5BD3278E
P 7000 3150
AR Path="/5BD3278E" Ref="U$?"  Part="1" 
AR Path="/5BD1C179/5BD3278E" Ref="U$2"  Part="1" 
F 0 "U$2" H 7000 3150 50  0001 C CNN
F 1 "282837-6" H 7000 3150 50  0001 C CNN
F 2 "RAMPS_1-41_eagle9:282837-6" H 7000 3150 50  0001 C CNN
F 3 "" H 7000 3150 50  0001 C CNN
	1    7000 3150
	1    0    0    1   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:+12V #P+?
U 1 0 5BD32794
P 6500 4450
AR Path="/5BD32794" Ref="#P+?"  Part="1" 
AR Path="/5BD1C179/5BD32794" Ref="#P+0127"  Part="1" 
F 0 "#P+0127" H 6500 4450 50  0001 C CNN
F 1 "+12V2" V 6400 4250 59  0000 L BNN
F 2 "" H 6500 4450 50  0001 C CNN
F 3 "" H 6500 4450 50  0001 C CNN
	1    6500 4450
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R?
U 1 0 5BD3279A
P 4700 2750
AR Path="/5BD3279A" Ref="R?"  Part="1" 
AR Path="/5BD1C179/5BD3279A" Ref="R13"  Part="1" 
F 0 "R13" H 4550 2809 59  0000 L BNN
F 1 "10r" H 4550 2620 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 4700 2750 50  0001 C CNN
F 3 "" H 4700 2750 50  0001 C CNN
	1    4700 2750
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_M0805 R?
U 1 0 5BD327A1
P 4700 3550
AR Path="/5BD327A1" Ref="R?"  Part="1" 
AR Path="/5BD1C179/5BD327A1" Ref="R14"  Part="1" 
F 0 "R14" H 4550 3609 59  0000 L BNN
F 1 "10r" H 4550 3420 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:M0805" H 4700 3550 50  0001 C CNN
F 3 "" H 4700 3550 50  0001 C CNN
	1    4700 3550
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R?
U 1 0 5BD327A8
P 4700 4350
AR Path="/5BD327A8" Ref="R?"  Part="1" 
AR Path="/5BD1C179/5BD327A8" Ref="R15"  Part="1" 
F 0 "R15" H 4550 4409 59  0000 L BNN
F 1 "10r" H 4550 4220 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 4700 4350 50  0001 C CNN
F 3 "" H 4700 4350 50  0001 C CNN
	1    4700 4350
	1    0    0    -1  
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:LEDCHIP-LED0805 LED?
U 1 0 5BD327AF
P 5600 4250
AR Path="/5BD327AF" Ref="LED?"  Part="1" 
AR Path="/5BD1C179/5BD327AF" Ref="LED2"  Part="1" 
F 0 "LED2" V 5740 4070 59  0000 L BNN
F 1 "LEDCHIP-LED0805" V 5825 4070 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:CHIP-LED0805" H 5600 4250 50  0001 C CNN
F 3 "" H 5600 4250 50  0001 C CNN
	1    5600 4250
	-1   0    0    1   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:LEDCHIP-LED0805 LED?
U 1 0 5BD327B6
P 5500 3450
AR Path="/5BD327B6" Ref="LED?"  Part="1" 
AR Path="/5BD1C179/5BD327B6" Ref="LED3"  Part="1" 
F 0 "LED3" V 5640 3270 59  0000 L BNN
F 1 "LEDCHIP-LED0805" V 5725 3270 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:CHIP-LED0805" H 5500 3450 50  0001 C CNN
F 3 "" H 5500 3450 50  0001 C CNN
	1    5500 3450
	-1   0    0    1   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:LEDCHIP-LED0805 LED?
U 1 0 5BD327BD
P 5500 2650
AR Path="/5BD327BD" Ref="LED?"  Part="1" 
AR Path="/5BD1C179/5BD327BD" Ref="LED4"  Part="1" 
F 0 "LED4" V 5640 2470 59  0000 L BNN
F 1 "LEDCHIP-LED0805" V 5725 2470 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:CHIP-LED0805" H 5500 2650 50  0001 C CNN
F 3 "" H 5500 2650 50  0001 C CNN
	1    5500 2650
	-1   0    0    1   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R?
U 1 0 5BD327C4
P 5800 4450
AR Path="/5BD327C4" Ref="R?"  Part="1" 
AR Path="/5BD1C179/5BD327C4" Ref="R23"  Part="1" 
F 0 "R23" H 5650 4509 59  0000 L BNN
F 1 "1.8K" H 5650 4320 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 5800 4450 50  0001 C CNN
F 3 "" H 5800 4450 50  0001 C CNN
	1    5800 4450
	-1   0    0    1   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R?
U 1 0 5BD327CB
P 5700 3650
AR Path="/5BD327CB" Ref="R?"  Part="1" 
AR Path="/5BD1C179/5BD327CB" Ref="R24"  Part="1" 
F 0 "R24" H 5550 3709 59  0000 L BNN
F 1 "1.8K" H 5550 3520 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 5700 3650 50  0001 C CNN
F 3 "" H 5700 3650 50  0001 C CNN
	1    5700 3650
	-1   0    0    1   
$EndComp
$Comp
L RAMPS_1-41_eagle9-eagle-import:R-US_R0805 R?
U 1 0 5BD327D2
P 5700 2950
AR Path="/5BD327D2" Ref="R?"  Part="1" 
AR Path="/5BD1C179/5BD327D2" Ref="R25"  Part="1" 
F 0 "R25" H 5550 3009 59  0000 L BNN
F 1 "1.8K" H 5550 2820 59  0000 L BNN
F 2 "RAMPS_1-41_eagle9:R0805" H 5700 2950 50  0001 C CNN
F 3 "" H 5700 2950 50  0001 C CNN
	1    5700 2950
	-1   0    0    1   
$EndComp
Wire Wire Line
	5600 4050 6300 4050
$EndSCHEMATC
